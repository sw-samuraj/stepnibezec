+++
title = "Vzestup ultra běžců"
date = 2020-12-20T20:29:28+01:00
tags = ["knihy", "ultra"]
description = """Adharanand Finn vydal třetí knihu, tentokrát
    o ultra běhání. Našel skrytý recept?"""
thumbnail = "2020/img/Rise-of-Ultra.jpg"
+++

{{< figure class="floatleft" src="/2020/img/Rise-of-Ultra.jpg"
    link="/2020/img/Rise-of-Ultra.jpg" width="300" >}}

[Adharanand Finn](//www.goodreads.com/author/show/1657627.Adharanand_Finn),
který je v Česku znám především svým bestsellerem [Běhání
s Keňany](//www.goodreads.com/book/show/25200859-b-han-s-ke-any),
vydal další knihu (česky zatím nevyšla): [The Rise of the Ultra
Runners](//www.goodreads.com/book/show/44162580).

Byť mě zmíněné _Běhání s Keňany_ nijak zvlášť nenadchlo, tak
na novou knihu jsem se těšil, protože číst si o ultra mě baví
a prozatím jsem si všechny knihy na toto téma užil a byly inspirativní.

A pak taky, načasoval jsem si čtení, aby předcházelo mému vlastnímu
ultra debutu. 😳

## Žijeme v době ultra maratonů?

> Maratony se staly malým úspěchem. Vypadá to, že jsme vstoupili do období
  "pouhý maraton".

Adharanand byl jako autor a běžec etablovaný o oblasti silničních závodů
a maratonů. Kromě _Kěňanů_ napsal i knížku o vytrvalostním běhání v Japonsku
([Cesta běžce](//www.kosmas.cz/knihy/224903/cesta-bezce/)).
A proto na začátku knihy vstupuje do oblasti ultra běhání jako naprostý
začátečník (ve smyslu znalosti domény).

Co autorovi slouží ke cti, jsou dvě věci:

1. Snaží se opravdu pochopit, o čem ultra běhání je, jak mu porozumět.
1. Vyzkouší si to na vlastní kůži --- během ultra závodů si mnohokrát
   sáhne na dno.

> Pro spoustu lidí je součástí přitažlivosti ultra běhání jeho nízkoúrovňový
  "Into the Wild" minimalismus, možnost ztratit se v divočině, překonat nejtežší
  a nejextrémnější prostředí na Zemi a to jen s láhví vody a bundou do deště.

Co se týká prvního bodu --- pochopení ultra běhání --- tam se Adharanand
opravdu snaží. Proč lidé vůbec něco takového absolvují? A co žene vpřed
ty elitní ultra hvězdy, které běhají na hranici lidských možností (pro většinu
lidí neuvěřitelných a nepřestavitelných)?

## Proč lidé běhají ultra?

Pochopení ultra běhání znamená hlavně se do něj ponořit --- absolvovat závody,
potkat komunitu a (v případě novináře) vyzpovídat ty úspěšné. Nebo aspoň ty,
co něčeho dosáhli (i když zůstali pod stupni vítězů).

Musím říct, že tak, jak na mne čtení knihy působilo, autor zůstal přes všechnu
svou snahu outisiderem --- nikdy tomu úplně nepropadl srdcem, pořád má takový
intelektuální odstup a i když si opakovaně prošel tím, čím každý správný
ultra běžec, tak se k jádru úplně nedostal. Nakonec zůstal novinářem, píšícím
o ultra běhání.

> "Dobrá práce, dobrá práce" říkají, jak mě míjejí. Každý to říká. Vím, že jsem
  protivný otrava, ale začíná mě to štvát. Nemůžou vymyslet něco jiného, co by
  říkali? "Dobrá práce, dobrá práce." Já nedělám dobrou práci. Já se držím
  za odrané špičky mých nehtů.

Autor zpovídá několik známějších ultra běžců, ale většinou to není úplně první
liga --- ačkoliv si o ultra už pár let čtu, většina zpovídaných jmen pro mě
byla neznámá. Což asi dává smysl 🤔 --- udělat interview s _Kilianem Jornetem_,
_Scottem Jurkem_, nebo s _Courtney Dauwalter_ nebude jednoduchý zorganizovat a
asi to ani nebude levný.

Pokud už člověk o ultra něco četl, tak tyto rozhovory moc nového nepřinesou.
Zajímavější je tak linie, kdy se autor účastní několika ultra závodů. A to vše
proto, aby získal kvalifikační body na [UTMB](//cs.wikipedia.org/wiki/Ultra-Trail_du_Mont-Blanc).
Protože hádejte, co bude finále knihy?

V tomhle se Adharanand velmi podobá své definici ultra běžce --- privilegovaný
bílý muž (žena), kteří mají dostatek prostředků a volného času na náročného
koníčka. Nejvíc to vyjde najevo právě u zmiňovaného UTMB. Ačkoliv je Adharanand
služný běžec (běhá líp než já), tak to není nic moc zvláštního, tudíž UTMB se
účastní jako obyčejný startující. Nicméně, vzhledem k tomu, že je novinář
[The Guardian](//www.theguardian.com/profile/adharanandfinn), tak má start jistý
--- nemusí do losovačky a stačí mu jenom nasbírat kvalifikační body.

## Patří _fell running_ do ultra?

Docela zajímavá je odbočka k [fell runningu](//cs.wikipedia.org/wiki/Fell#Britsk%C3%A9_ostrovy),
specifické odnože trailového běhání, která je domovem na britských ostrovech.
Osobně jsem nikdy nechápal fell running jako součást ultra běhání,
ale budiž. 🤷‍♂️

Ale to nevadí, protože fell running je zábavný a zajímavý (takový můj sen
--- proběhnout s v [Lake District](//cs.wikipedia.org/wiki/Jezern%C3%AD_oblast)).

> Ukazuje se, že součástí schopností a výzvy fell runningu je vybrat si svoji
  trasu skrze kameny, prolákliny a příkré úbočí kopců. Pouze někoho následovat
  není úplně v duchu téhle záležitosti.

## Měli by Keňané běhat ultra?

Dost úsměvný mi přijde autorův pokus vymanit ultra z dominance bílých závodníků.
Pozastavuje se nad tím, že ultra neběhají skoro žádní černoši a zejména, proč
by ho něměli běhat (a vyhrávat) jeho oblíbění Keňani? 🤦‍♂️

Dokonce učiní pokus jednoho Keňana zasponzorovat, aby si ultra vyzkoušel.
Nijak překvapivě to skončí fiaskem. Keňani prostě nechápou, proč by měli
běhat víc, než maraton (vlastně si to ani neumí představit a spočítat).
Navíc, když v tom nejsou žádné prize money.

A tak ultra zůstává dál doménou euro-americké (a trochu japonské) civilizace.

## Suma sumárum

Musím říct, že až doposud mě knihy o ultra inspirovaly. Tohle je první kniha,
která mě neinspirovala ani trochu... protože v ní nebylo nic nového, co bych
už neznal a autorův lehce odtažitý styl mi úplně nesedí.

To ale neznamená, že je špatná --- je to fajn čtení a pokud jste o ultra
ještě nic nečetli, tak to možná není úplně špatný úvod do této domény.
Já sice dávám přednost větším srdcařům, ale i tak, rozhodně to byl příjemně
strávený čtenářský čas.

> Vypadá to, že bez ohledu na to, jak jste dobrý, tyhle závody nejsou nikdy
  snadné. Ale je to jejich epická, drsná přirozenost, způsob, kterým vás nechají
  si sáhnout na dno, co je dělá tak jedinečnými.

_Pozn.: Četl jsem v angličtině - uvedené citáty jsou mým volným překldem._

## Související články

* [Běhání s Keňany](/2020/behani-s-kenany/)
