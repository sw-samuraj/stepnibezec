+++
title = "Černá Hora SkyRace, co tě nezabije..."
date = 2020-09-11T15:57:23+02:00
tags = ["trail", "sky", "závody", "krkonoše"]
description = """Je to drsnej závod. Už jsem ho po loňsku nechtěl znovu
    běžet... 🤭"""
thumbnail = "2020/img/cerna-hora/Finish.jpg"
+++

{{< figure src="/2020/img/cerna-hora/Finish.jpg"
    link="/2020/img/cerna-hora/Finish.jpg" >}}

Protože nejsem žádné B-čko a moje (běžecké) ambice (občas) neznají mezí
(to je tak, když člověk furt kouká na [YouTube
na Kiliana](//www.youtube.com/results?search_query=kilian%20jornet)
🤦‍♂️), přihlásil jsem se (znovu) na závod [Černá Hora
SkyRace](//cernahora-skyrace.cz/). A vlastně jsem to udělal kvůli jediné
věci --- letos totiž oproti loňsku dávali tričko. A to se vyplatí,
zmasakrovat si nohy kvůli kusu textilu s logem závodu. 🤪

Tož, to bylo tak...

## Ambice nezkušeného skyrunnera

Tenhle závod už jsem nechtěl znovu běžet. Loni, kdy proběhl nultý ročník,
mě nadchla idea zaběhnout si skyrunningový závod, který je delší, než
známý [Ještěd SkyRace](//www.jestedskyrace.cz/). Ten už jsem běžel dvakrát.
Stejně tak jsem dvakrát běžel [Jesenický maraton](//jesenickymaraton.cz/).
Takže zaběhnout si třicítku, to už bude brnkačka, ne? Ach, já pošetilec!

Závod jsem sice absolvoval (a na svou výkonnost bych řekl, že i se ctí),
ale zrušil jsem se přitom tak, že jsem na 3-4 měsíce přestal běhat.
Samozřejmě mě vytrestala moje lehkovážnost.

V první řadě, jde o horský závod a tak je potřeba počítat s určitou
soběstačností --- v pití, v jídle, atd. Už si přesně nepamatuju rozložení
občerstvovaček, ale předpokládám, že to bylo podobně, jako letos ---
byly tři, přičemž největší vzdálenost mezi nima byla nějakých 14 km.
To si moc dobře pamatuju --- byl jsem dehydrovaný a po hrstech jsem spásal
obrovské borůvky, které rostly na odvrácené straně Černé hory. Bez nich
bych nepřežil. 💀

A pak, podcenil jsem profil: 1.800 m převýšení jsou prostě téměř dva vertikální
kilometry. To neošidíš. Bohužel, člověk si to nedokáže představit, dokud
v tom kopci nestojí. Dnes už vím, že jít takové stoupání bez hůlek je buď
masochismus, nebo sebevražda. Závěrečné 4km klesání do cíle jsem protrpěl
stylem gejša: cupity, cupity. Bolelo to. 😣

Jedna z mála věcí, na které jsem při svém běhání hrdý, je, že nemám žádný
[DNF](//en.wikipedia.org/wiki/Did_Not_Finish). A tak i tenhle skyrace jsem
dokončil. A ihned jsem ho zařadil mezi nejtěžší závody, které jsem kdy
běžel.

## Opatrnost přeživšího

Když jsem letos v únoru, v březnu plánoval sezónu, tak jsem s _Černou Horou_
vůbec nepočítal --- naplánoval jsem si maratony, krátký traily a taky svůj
první (baby) ultra. Jenže znáte to, jak je to s těmi plány...

Takže potom, co mi zrušili již posunutý maraton, jsem se opět ponořil
do plánování. Už ani nevím proč jsem se znovu podíval na stránky
_Černé Hory_ a ejhle! Dávali triko. 🤦‍♂️ Tak jsem se přihlásil.

Ovšem protože v půlce prázdnin jsem měl naplánovanou [Krkonošskou
padesátku](//www.trailrunningcup.cz/sezona-2020/krkonose-maraton/),
měl jsem na horské závody rozumně natrénováno (viz článek [Příprava
na první ultratrail](/2020/priprava-na-prvni-ultratrail/)). A v tomhle světlě
už se _Černá Hora_ nejevila tak černě. 🤭

A kromě specifického tréninku zde bylo také poučení z krizového vývoje.
Shrnul bych to lakonicky jako: hůlky, vesta, gely.

* Hůlky, protože převýšení.
* Vesta, protože pití a výbava.
* Gely, protože energie.

Zbytek rovnice jsou už jenom: nohy, plíce, hlava. ✅

## ... to tě posílí

{{< figure src="/2020/img/cerna-hora/On-the-road.jpg"
    link="/2020/img/cerna-hora/On-the-road.jpg" >}}

Podobně jako loni, jsem lehce po 7 ráno vyrazil z Prahy a na parkoviště
v Jánských Lázních dorazil kolem 9:15. Registrace byla rychlá --- dlouhý
závod běželo něco přes sto lidí, takže žádné fronty.

Převlíkl jsem se do běžeckého a už jenom u toho jsem propotil triko!
Bylo nezvykle teplo, dobře přes 20 °C. Loni mlha a sychravo, letos
letní trailík. Hory bývají vrtkavé.

V netradiční čas 9:55 se štrůdlík skyrunnerů dal do pohybu směr Černá
hora. Kam jinam než rovnou do svahu. Eliťáky jsem ani nezahlídl a pěkně
jsem to umírněně jistil zezadu, z naprostého chvostu --- poučen z loňska
jsem věděl, že tady, v prvním kopci, se chleba rozhodně neláme a zrno se
oddělí od plev mnohem, mnohem později.

Po prvním kilometru jsem vytáhnul hůlky a svižným skyrunnerským krokem
jsem se po 5 km a 600 m převýšení poprvé podíval na vrcholek Černé hory.
Oproti loňsku byl prohozený start a doběh, ale nijak zásadní rozdíl jsem
v tom neviděl.

Kousek za vrškem, u Černé Boudy byla na 6. km první občerstvovačka
a taky se zde dělila dlouhá a krátká (12 km) trasa. Doplnil jsem do softflasků
vodu, protože další občerstvovačka byla až na 20. km. Čekal mě krásný,
dlouhý 6km seběh severní stranou Černé hory, s pěknými výhledy na Sněžku
a masiv Luční hory.

{{< figure src="/2020/img/cerna-hora/Snezka.jpg"
    link="/2020/img/cerna-hora/Snezka.jpg"
    caption="Severní úbočí Černé hory s výhledem na Sněžku" >}}

Tady jsem si na 8. km dal první gel, ty další jsem si už dávkoval po 6 km.
Nikam jsem se nehnal, držel jsem si své tempo a i když seběhy všeho druhu
mi jdou, nijak jsem to netlačil. Buď jsem už starý, nebo rozumný. 🤷‍♂️

Následující kopec, okolo Jeleního vrchu, byl mírný a krátký --- kilometr
na délku a 200 m stoupání. Já si jedu svoji klasiku: do kopce chůze
s hůlkama, na rovině a z kopce běh. Jakmile seběhneme 1,5 km (a ztratíme
nějakých 300 m), jsme v půlce závodu. A jak jinak, další kopec --- 2,5 km
na Špičák a dalších 300 m výškových.

Teď nás čeká seběh na kraj Černého Dolu, kde je na 20. km druhá občerstvovačka
a já udělám první chybu --- doplním si jenom jednu flašku, 1/2 litru vody mi
do cíle bude stačit. Chyba, chyba... už jsem zapomněl, co nás čeká.

Protože, je to přece SkyRace, že jo! Takže znovu na Černou horu, opět přes
600 m převýšení, ale tentokrát ne na 5 km, jako na začátku. Ne, ne. Tentokrát
je to převýšení na pouhých 3,5 km. Tady se lámou charaktery. Tady nezkušení
proklínají své ambice a zkušení tiše trpí.

Já jsem trpěl tiše, podporován hůlkmi --- v poslední třetině pod vrcholem
už to byl boj. Zejména ten nekonečný kamenný chodník se schody! Nicméně,
hůlky a gely mne podržely. Takže předcházeje soupoutníky jsem čas od času
utrousil povzbudivou větu.

{{< figure src="/2020/img/cerna-hora/Stone-trail.jpg"
    link="/2020/img/cerna-hora/Stone-trail.jpg"
    caption="To nejsou schody do nebe, to je chodník smrti --- výstup na Černou horu z Černého Dolu" >}}

Někde v půlce výstupu jsem udělal druhou chybu --- byla tam studánka
a já jsem si opět(!) nedoplnil vodu. 🤦‍♂️ Každopádně, bojoval jsem
dál, dokonce jsem na to v druhé půlce nebyl sám --- v takovém krpálu
je spoluběžec schopný konverzace vítanou vzpruhou a s kolegou jsme to
vytáhli až na vrchol. Jen abyste měli představu, o čem mluvím --- tady
jsou ty tři kritické kilometry závěrečného výstupu. Podívejte se zejména
na tempo a kadenci, to hovoří za vše. 😈

| Km  | Pace  | Elev. gain | Cadence |
|-----|-------|------------|---------|
| 21. | 15:54 | 183        | 65      |
| 22. | 18:56 | 205        | 41      |
| 23. | 24:13 | 230        | 19      |

Ano, poslední kilometr na Černou horu jsem šel téměř 25 minut, s kadencí
19 kroků za minutu! Pravda, úplně to matematicky nevychází, ale co
Garmin naměřil, to naměřil. 🤷‍♂️

Jakmile jsme s kolegou byli nahoře, čekala nás jen kilometrová rovinka
k poslední občerstvovačce (opět ta samá u Černé Boudy). Tady jsem se pár
minut zdržel (a kolega mě opustil) --- čekal mě už jen 6km seběh do cíle.
Pořádně jsem se občerstvil a doplnil jednu flašku.

Tady mám jeden občerstvovací tip, který jsem se naučil na _Krkonošské
padesátce_: namočit oloupaný banán do soli, sníst a zapít vodou, nebo
ionťákem. Divná kombinace, ale kdekoliv po 25. km vám bude připadat
lahodná. 🤓

Závěrečný seběh byl dlouhý. Naštěstí jsem oproti loňsku měl zachovaný
stehna, takže jsem nemusel cupitat jak gejša, 🎎 ale běžel jsem vcelku
rozumných 5-6 minut na kilometr. Ale bylo to dlouhý, ubíhalo to pomalu.

Do cíle jsem zkusil ještě zabojovat, abych to dal pod 4,5 hodiny, ale
už to nešlo. Nakonec jsem proběhl cílem v oficiálním čase 4:31:30, dvě
hodiny a osm minut za vítězem.

Celkově dlouhý závod dokončilo 108 lidí, já jsem byl 89. Loni dokončilo
82 lidí a já jsem byl 63. Je určitě jen dílem náhody, že jsem v obou
případech skončil 19. od konce. 🤭

{{< figure src="/2020/img/cerna-hora/Elevation.jpg"
    link="/2020/img/cerna-hora/Elevation.jpg" >}}

## Tak co, ještě jednou?

Celkově mám ze závodu jen dobré pocity --- zlepšil jsem si čas o 23
minut, v ideálním prostředí si vyzkoušel hůlky, vestu a gely. Nestrhal
jsem se. Co se týče zkušeností, nebál bych se u podobného závodu
uštědřit radu začátečníkovi.

Ale hlavně --- je to náročný závod v horách! A to mě baví. 🤗 Jestli
půjdu za rok znovu? Nevím. Září bývá na závody bohaté a já většinou
stejné závody běhám opakovaně jen výjimečně. Ale _Černá Hora SkyRace_
by se mohla zařadit mezi moje oblíbené.

{{< figure src="/2020/img/cerna-hora/Shoes.jpg"
    link="/2020/img/cerna-hora/Shoes.jpg" >}}

## Související články

* [Krkonošská padesátka, můj první (baby) ultra trail](/2020/krkonoska-padesatka/)
* [Příprava na první ultratrail](/2020/priprava-na-prvni-ultratrail/)
