+++
title = "Sněžka 25k"
date = 2020-11-17T16:36:03+01:00
tags = ["trail", "vrcholy", "krkonoše"]
description = """Byl to takový drobný sen - vyběhnout si na Sněžku. ✅"""
thumbnail = "2020/img/snezka/Hrebenovka.jpg"
+++

{{< figure src="/2020/img/snezka/Hrebenovka-header.jpg"
    link="/2020/img/snezka/Hrebenovka-header.jpg" >}}

Když nejsou závody, tak si člověk něco vymyslí. I když tohle nebyl
úplně takový ten osobní projekt, který vyplní covidové vacuum. To
bylo tak...

Někdy před devíti lety, když dceři ještě nebyly ani dva roky, jsme
vyrazili na rodinný výlet na Sněžku. Dceru jsem nesl v krosničce.
Bylo to v dobách, kdy jsem se začal zajímat o trailové běhání.

{{< figure src="/2020/img/snezka/Vrtulnik.jpg"
    link="/2020/img/snezka/Vrtulnik.jpg"
    caption="Prý na svážení klestí (Richtrovy boudy)" >}}

Vyšlápli jsme to nahoru a najednou, na vršku, vidím, jak po té
kamenné "silničce", co vede z Polska, vybíhá nahoru holka...
a na nohou má Inov-8ty, takový ten legendární modro-oranžový
model X-Talonů.

Od té doby jsem věděl, že si chci na Sněžku vyběhnout taky.
A i když to byl jen takový malý sen, běžec s rodinou si musí
na svou příležitost občas počkat...

{{< figure src="/2020/img/snezka/Vyrovka.jpg"
    link="/2020/img/snezka/Vyrovka.jpg"
    caption="Výrovka v husté mlze" >}}

Už jsem o tom letos psal v článku [Příprava na první
ultratrail](/2020/priprava-na-prvni-ultratrail/) --- začal jsem si
v [Mapy.cz](//mapy.cz) naklikávat trailové trasy, které jsem si
pak nahrával do hodinek a běhal podle navigace.

Tyhle trasy většinou běhávám s běžeckým parťákem a jednou jsme
si tak při běhu povídali o Krkonoších a že bychom se rádi proběhli
po krkonošském hřebenu... a tak jsem si naklikal pár
tratí na Sněžku. Ta nejdelší je 50km okruh z Černé hory na Sněžku
(a zpět), ta střední je 35 km z Pece na Sněžku a ta nejkratší totéž,
ale jen milosrdných 25 km.

{{< figure src="/2020/img/snezka/Namraza.jpg"
    link="/2020/img/snezka/Namraza.jpg"
    caption="Námraza na smrcích (cestou na Luční)" >}}

Bylo jen otázkou času, než na jednu z nich dojde. A tak jsme si vzali den
dovolený a páteční dopoledne nás zastihlo na parkovišti v Peci
pod Sněžkou. Předpověď slibovala v Peci 5 °C maximum a polojasné
slunečno.

Vyrazili jsme po červené do Modrého dolu. Čekalo nás vytrvalé
7km stoupání až na Luční horu. Po prvním kilometru jsem vytáhnul
hůlky, které jsem nepustil z ruky až ke Kapličce mezi Luční a
Studniční horou.

{{< figure src="/2020/img/snezka/Na-Lucni.jpg"
    link="/2020/img/snezka/Na-Lucni.jpg"
    caption="Cestou na Luční boudu" >}}

Tady vám musím něco prozradit o svém běžeckém parťákovi --- je to
zkušený a letitý skialpista. Takže všechny ty kopce, co já chodím,
nebo běhám s hůlkama... on prostě běhá (a nahoře na mne čeká).

Po odbočce z Modrého dolu na Richtrovy boudy nás na chvilku zaskočila
páska se zákazem, ale nakonec se ukázalo, že je to jen kvůli svážení
klestí vrtulníkem. Ten jsme za chvíli i viděli --- parkoval hned vedle
Richtrových bud.

{{< figure src="/2020/img/snezka/Snezka.jpg"
    link="/2020/img/snezka/Snezka.jpg"
    caption="Sněžka v mracích" >}}

Další kopec na Výrovku nás ponořil do mlhy, ale už při výstupu
na planinu Luční hory, lemovaným namrzlými smrky, se nám ukázala
příjemná, slunečná tvář hor. Lidí jsme potkávali málo, běželo se
po rovince.

Někde u Luční boudy jsem si dal první gel --- běželi jsme nalehko.
V baťůžku jsem měl jen rukavice, čepici a náhradní triko, kdyby
na Sněžce bylo ošklivě mrazivo.

{{< figure src="/2020/img/snezka/Polsko.jpg"
    link="/2020/img/snezka/Polsko.jpg"
    caption="Pohled do Polska" >}}

Ještě nikdy jsem nabyl na polské straně hor, tak jsem tam trasu
při plánování trochu protáhnul. Cesta byla kamenitá, dlážděná,
běželo se dobře. Za chvíli jsme byli u západního nástupu na Sněžku,
kde se připojuje cesta z Obřího dolu. Lidí znatelně přibylo.

Vrchol Sněžky se přechodně schovával v mracích a my jsme se rozhodli
pro kamenou cestu na polské straně. Jednak jsme tam ani jeden z nás
ještě nebyli, jednak tam bylo málo lidí.

{{< figure src="/2020/img/snezka/Snezka-kameni.jpg"
    link="/2020/img/snezka/Snezka-kameni.jpg"
    caption="Sněžka je plná kamení" >}}

Samozřejmě bych mohl chlapácky tvrdit, že jsem na tu Sněžku vyběhnul.
Ale pravda je, že jsem ji zdolal svým klasickým, skyrunnerským stylem
--- s hůlkami, občas jít, občas popoběhnout. Je to prostě krpál. Ale
můj parťák to vyběhnul celý! Čestný pionýrský.

Na vrchol jsme dorazili v pravé poledne (i když to podle fotky vůbec
nevypadá), dali si sušenku, banán a samozřejmě, vrcholovou fotku.
Můj parťák dorazil chvilku přede mnou, takže se hnedka zakecal
s další běžeckou dvojicí. Prý nás dokonce někde zahlédli.

Výhledy nebyly, trošku profukovalo, výš už to nešlo, co jsme mohli
dělat? Tak jsme po svačince pokračovali dál.

{{< figure src="/2020/img/snezka/Na-vrcholu.jpg"
    link="/2020/img/snezka/Na-vrcholu.jpg"
    caption="Na vrcholu v pravé poledne (aneb metafora běžeckého stylu)" >}}

Hřebenovka na východ od Sněžky mě vždycky lákala a dnes jsem na ni
poprvé mohl spočinout svou nohou. Nutno říct, že sny jsou někdy lepší
než skutečnost --- kamenný chodníček není úplně to, co by moje běžecká
inkarnace milovala.

Přece jenom, správný trail je pro mne nezpevněná lesní cesta, či
pěšinka. Těch v Krkonoších moc není --- Krkonoše, to jsou hlavně
silničky a kamenné chodníčky. Na turistiku asi dobrý, na běhání
nic moc. Ale za ty rozhledy, okolí a čerstvý vzduch to stojí.

{{< figure src="/2020/img/snezka/Hrebenovka.jpg"
    link="/2020/img/snezka/Hrebenovka.jpg"
    caption="Hřebenovka (ze Sněžky na východ po červené)" >}}

Po Obřím hřebeni jsme dorazili na Svorovou horu a u boudy Jelenka
jsme to po zelené otočili zpátky na západ, zpět ke Sněžce. Tady se
ukázaly limity plánování nad mapou z tepla domova. Byť je stezka
nad Slunečním údolím značená turistická trasa, běžet se nedala.

Já vím, že [Kilian](//en.wikipedia.org/wiki/K%C3%ADlian_Jornet_Burgada)
by to samozřejmě běžel jako srnka, ale já jsem po 15 km pokorně vzal
hůlky a přešel do chůze --- pěšina byla plná kořenů, kamenů, výmolů,
nahoru dolů, občas přechod kamenného pole...

{{< figure src="/2020/img/snezka/Slunecne-udoli.jpg"
    link="/2020/img/snezka/Slunecne-udoli.jpg"
    caption="Slunečné údolí" >}}

V takových místech mám jednoducho strategii: neplýtvat energií a
nezranit se. Jakákoli pomoc je sakra daleko. Ani můj parťák, který mě
obvykle podporuje a popohání ať běžím, nic nenamítal.

Buď jak buď, ve Slunečném údolí bylo opravdu slunečno. Nicméně hned, jak
jsme se pod lanovkou napojili na žlutou směr Růžová hora a Růžohorky,
zapluli jsme do mlhy, která nás více méně neopustila až do cíle.

{{< figure src="/2020/img/snezka/Jelenka.jpg"
    link="/2020/img/snezka/Jelenka.jpg"
    caption="Jelenka (po zelené), pohled na Sněžku" >}}

Zpátky do Pece jsme doběhli v čase 4 hodiny a 3 minuty. Mapy.cz
trochu kecali --- i když moje Garminy trochu podměřují, tak to nebylo
25 km, ale téměř 27. Nasbírané převýšení 1.310 m. Mně na to stačily
dva gely, jedna sušenka, trochu vody, větrovka a dobré trailové boty.

Byl to pěkný běh. Už plánuju v Krkonoších další. Letos to asi už
nebude, ale na jaře...

{{< figure src="/2020/img/snezka/Elevation.jpg"
    link="/2020/img/snezka/Elevation.jpg" >}}

* Trasa na Mapy.cz: [Sněžka 25k](//mapy.cz/s/cufenuzejo)
