+++
title = "Moje maratonské vyznání"
date = 2020-05-10T17:16:52+02:00
tags = ["maraton"]
description = """Uběhnul jsem maraton. A pak další. A další. Stala se z toho
    když ne vášeň, tak potřeba a radost. Jak jsem se k tomu dostal?"""
thumbnail = "2020/img/Marathon-legs.jpg"
+++

{{< figure src="/2020/img/Marathon-legs.jpg" link="/2020/img/Marathon-legs.jpg" >}}

Jednoho léta jsem se zvedl z gauče a po osmi měsících a 800 kilometrech
jsem ve čtyřiceti zaběhl svůj první maraton.

## Dětství a mládí

V dětsví a mládí jsem rozumně sportoval --- od kolektivních po individuální
sporty --- a i když jsem byl občas lehce nadprůměrný, nikdy jsem nic
nevyhrál a nikdy to nikam nedotáhl.

Ještě na základce jsem chodil rok do atletického oddílu, tak 7-8 třída. Byl
jsem s nimi jednou na závodech: postavili mě na start 1500 m, nic mi
k tomu neřekli, vyrazil jsem jako šíp a poté, co mě v následujících kolech
postupně všichni předběhli, jsem potupně doběhnul jako poslední.
Přestal jsem tam chodit.

Sport, u kterého jsem vydržel nejdýl, je pro mne dodnes srdcovou
záležitostí: baseball. Hrál jsem ho nějakých 6-8 let (už si to přesně
nepamatuju) a potáceli jsme se na chvostu krajské ligy. Hrál jsem
v zadním poli, jako jediný v týmu jsem dokázal naštorc přehodit celé
fotbalové hřiště. Říká se, že podle statistik dá průměrný baseballista 3
[homeruny](//cs.wikipedia.org/wiki/Homerun) za život. Já jsem dal 3 homeruny
v jedné sezóně a s baseballem jsem nadobro skončil. Bylo mi nějakých
26 let.

## Sladkých 30

Celá svoje 30. léta jsem věnoval kariéře --- po nocích jsem programoval
a četl spoustu knih. Výsledek se po letech projevil... vyhřezla mi plotýnka.

Těsně, než k tomu došlo jsem měl touhy začít zase rekreačně sportovat.
Přibral jsem 20 kilo. Sice jsem trochu víkendově jezdil na kole, ale nebylo
to úplně ono. Snil jsem o běhání --- v dětství a mládí jsem běhal rád
(i ve volném čase) a tak mi to přišlo jako nejpřirozenější.

Vyhřezlá plotýnka není legrace --- naštěstí jsem nemusel na operaci,
protože jsem prošel zázračnou rehabilitací, ale byl jsem rád, že můžu
aspoň chodit.

Narodilo se nám první dítě a já jsem si koupil ve výprodeji první běžecké
boty. Začal jsem postupně obden běhat 4-6 kilometrů. Pak se nám narodilo
druhé dítě a já jsem na dva tři roky přestal sportovat úplně. Nebyly
na to síly.

## Čtyřicítka na krku

Maraton a půlmaraton jsem měl na seznamu už řadu let, ale už dávno jsem je
odsunul do vzdálené budoucnosti. Bylo mi 39 a vážil jsem o 30 kg víc,
než když jsem se seznámil se svojí ženou.

To léto jsem začal znovu běhat, většinou kolem 6 km a bylo to poprvé,
kdy jsem si své běhání měřil --- pomocí chytrého telefonu. Musím říct,
že ty běžecký statistiky mě nadchly. Začal jsem běhat víc a víc.

A pak jsem se v září, v momentu přehnané sebedůvěry, přihlásil
na Pražský maraton. Měsíc na to jsem si koupil první běžecký
hodinky: _Garmin Forerunner 110_.

Běhal jsem, běhal a měsíc před svými 40. narozeninami jsem nastoupil
maratonský plán, který jsem našel on-line na stránkách
[RunKeeperu](//runkeeper.com/home). Plánu jsem se držel, jen co
se týká kilometráže --- tempa, ani typy běhů jsem vůbec neřešil.

## Maraton a co dál?

A pak jsem v květnu odběhnul svůj první maraton. Na konci jsem
žádnou euforii necítil. Ale po pár dnech, kdy už mě přestávaly
bolet nohy, jsem najednou něco cítil. Uvnitř jsem cítil, že to
chci prožít znovu. A tak jsem na podzim běžel další maraton.

Pak následovaly další maratony a já jsem si postupně ustanovil
následující tři cíle --- jeden krátkodobý, jeden střednědobý a
jeden dlouhodobý:

1. Že bych chtěl pravidelně běhat 2 maratony ročně.
1. Že bych chtěl zaběhnout limit na [Bostonský marathon](//www.baa.org/races/boston-marathon).
1. A že bych chtěl zaběhnou maraton, až mi bude 80 let.

Kdo ví, jak moje plány dopadnou. Ale pravdou je, že u běhání
jsem vydržel zatím nejdéle, ze všech sportů, co jsem kdy dělal.
Přišlo to pozdě --- svůj statisticky nejrychlejší maraton jsem
prošvihnul o nějakých 20 let --- ale cítím, že to výrazně definuje moji
identitu. A ještě nějaký čas bude.
