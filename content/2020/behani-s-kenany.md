+++
title = "Běhání s Keňany"
date = 2020-04-19T20:00:02+02:00
tags = ["knihy", "maraton"]
description = """S Keňany už jsem párkrát běžel. Jaké je jejich tajemství?
    Dá se vyčíst z knihy se stejnojmenným názvem?"""
thumbnail = "2020/img/Running-with-the-Kenyans.jpg"
+++

{{< figure class="floatleft" src="/2020/img/Running-with-the-Kenyans.jpg"
    link="/2020/img/Running-with-the-Kenyans.jpg" width="300" >}}

Taky už jsem párkrát běžel s Keňany. 🤭 Pokud běžíte nějaký
[RunCzech](//www.runczech.com/) závod, tak se tomu nevyhnete.
A pokud běháte tak jako já (tj. 2x pomaleji, než ti Keňané) a někde na trati
je obrátka, tak je cca někde v půlce potkáte, jak běží v protisměru. 😂

To že Keňané (ale bacha na to! - i Etiopané, Eritrejci a Uganďané) dominují
vytrvalostním běžeckým disciplínám, si za poslední dlouhé roky všimnul kde
kdo. A jak už to tak bývá, ve světě si toho všimli dřív, než u nás v Česku.
No a [Adharanand Finn](//www.goodreads.com/author/show/1657627.Adharanand_Finn)
o tom před lety napsal knihu [Running with the
Kenyans](//www.goodreads.com/book/show/19268021-running-with-the-kenyans),
která pak vyšla před časem i u nás pod názvem [Běhání
s Keňany](//www.goodreads.com/book/show/25200859-b-han-s-ke-any).

## Je v tom nějaké tajemství?

Podtitul českého vydání je _Tajemství nejrychlejších běžců světa_. Jak
už to u těch "odhalených" tajemství bývá, neexistuje žádná stříbrná kulka,
žádný zázračný recept, který vyrobí úspěch. Je to spoustu drobných věcí,
které zapadnou do sebe a pokud je někdo (po mnoho let) ve správný čas
na správném místě, respektive se na tom místě narodí, či žije převážnou
část života, může se na tom úspěchu podílet.

> Lidé se během miliónů let vyvinuli v běžce nikoli proto, že to byla
  zábava, ale proto, aby přežili.

Co všechno je pravděpodobně v tom koktejlu? Nadmořká výška
([Iten](//en.wikipedia.org/wiki/Iten), kde se kniha odehrává je 2.400 m n.m.),
běhání přes špičku, běžecké kempy, výchova a vyrůstání v chudých poměrech,
inspirující vzory/idoly, strava, vyzkoušená cesta k zajištěnému životu,
všudypřítomná běžecká kultura a zkrátka a jednoduše --- úcta k běhání.

> Kdykoli běžím se skupinou Keňanů a přiběhneme k nějakému kopci, tak oni
  všichni zrychlí, zatímco moje přirozená inklinace je zpomalit. Když oni
  vidí kopec, vidí ho jako příležitost. Příležitost tvrději trénovat,
  tvrději makat.

## Jak trénoval Adharanand?

{{< figure class="floatright" src="/2020/img/Behani-s-Kenany.jpg"
    link="/2020/img/Behani-s-Kenany.jpg" width="200" >}}

Kromě linky, která se snaží zodpovědět, proč jsou Keňané v běhání tak
dobří, je zde ještě další, kde se autor snaží běžecky zlepšit a zaběhnout si
svůj první maraton ([Lewa Marathon](//www.lewasafarimarathon.com/)).

Přiznám se, moc jsem z knihy nepochopil, jestli měl Adharanand nějaký
strukturovaný, či promyšlený trénink --- mě to přišlo, že se čas od času
přilepil k nějaké běžecké skupině, občas ho někdo na běh pozval, nebo měli
pár výběhových tréninků se skupinou, kterou tam založil: _Iten Town Harriers_.
A jednou běžel [fartlek](//en.wikipedia.org/wiki/Fartlek) (30x minuta).
Možná, že se jen přizpůsobil lokálním způsobům.

> Nikdo si nevede tréninkový deník, ani nepočítá týdenní objemy. Každá
  session je zapomenuta, jakmile je odbyta. Měření času je pouze způsob
  strukturování tréninku, kdy začít a kdy skončit.

## Jsou Keňani ještě inspirativní? 🤔

Adharanand knihu publikoval v roce 2010. Dnes, deset let poté už je situace
jiná --- spoustu běžců jezdí do Itenu rutinně trénovat, leckteré závody se
snaží zvaní Keňanů vyhýbat a preferují "local heros" a ačkoli stále podávají
Keňani a jiní východo-afričani fantastické výkony, jsou jednoduše... zaměnitelní.

Kniha tedy trochu zestárla (a dobře to reflektuje doslov ke 2. anglickému
vydání). I tak, _Running with the Kenyans/Běhání s Keňany_ je fajn běžecká kniha,
kterou stojí za to si přečíst.

> Po šest měsíců jsem dával dohromady hádanku, proč jsou Keňani tak dobří
  běžci. Na konci tu nebyl to žádný elixír, žádný běžecký gen, žádné tréninkové
  tajemství, které byste mohli pěkně zabalit a prezentovat se vší slávou.
  Nic, co by Nike mohl zkopírovat a prodávat jako nejaktuálnější běžecký trend.
  Ne, bylo to příliš komplexní a zároveň příliš jednoduché. Bylo to všechno
  a nic.

_Pozn.: Četl jsem v angličtině - uvedené citáty jsou mým volným překldem._
