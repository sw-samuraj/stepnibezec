+++
title = "Příprava na první ultratrail"
date = 2020-06-21T14:09:56+02:00
tags = ["ultra", "trail", "trénink"]
description = """V létě mě čeká první ultratrail, Krkonošká padesátka.
    Jak se na něj připravuji? Jaké vybavení jsem si pořídil? A běhám
    nějak specificky?"""
thumbnail = "2020/img/Ultratrail.jpg"
+++

{{< figure src="/2020/img/Ultratrail.jpg" link="/2020/img/Ultratrail.jpg" >}}

Už léta mě fascinují ultramaratony. Zatím si o nich jen čtu --- takové ty
klasiky: [Scott Jurek](//www.goodreads.com/book/show/14922390), [Kilian
Jornet](//www.goodreads.com/book/show/22886764) a [pár
dalších](https://www.goodreads.com/review/list/25024972-v-t-kota-ka?page=1&shelf=running%2Cread).
Aktuálně se chystám na novinku od [Adharananda
Finna](https://www.goodreads.com/book/show/44162580).

Jenže... něco jiného je si o tom číst a něco jiného si to zkusit. Pár
maratonských zkušeností už mám, tak proč se neposunout kousek dál?
Letos jsem se rozhodl do toho praštit a zkusit si svůj první ultratrail.

## Závod

Řekl jsem si: "nebudu to pro začátek přehánět" a vybral jsem si něco
menšího, takové "baby-ultra": [Krkonošskou
padesátku](//www.trailrunningcup.cz/sezona-2020/krkonose-maraton/). Proč
zrovna tohle?

V první řadě jsem chtěl něco v horách --- že bych plácal něco po silnici
mě ani na okamžik nenapadlo. Zkrátka v horách je ta bolest tak nějak
snesitelnější.

Chtěl jsem něco kratšího, abych si neukouslnul moc velké sousto ---
tudíž trailová padesátka, je zatím tak optimální.

A konečně, chtěl jsem něco v půlce léta. Původně jsem plánoval jeden
jarní maraton (to úplně nevyšlo, koronavire!) a jeden podzimní.
A protože neběhám nijak šílené objemy, tak to musí do sebe nějak
zapadnout --- abych stačil natrénovat, zregenerovat atd. To je vždycky
v řádech týdnů.

A pak taky, profilem a převýšením se to hodně podobá [Jesenickému
maratonu](//jesenickymaraton.cz/), takže bych to mohl
rozumně zvládnout a nebude to tak velký skok do neznáma.

## Výbava

### Oblečení

Oblečení neřeším --- zatím jsem všechny horské běhy odběhnul ve věcech
z _Tchiba_, v tričkách, co jsem nasbíral po různých závodech a v kraťasech
a elasťákách ze _Sportisima_.

Jedinou závažnější věcí jsou ponožky --- momentálně jsem dotrhal svoje
oblíbené _Salomony_, které perfektně seděly a netrhaly se(!) a tak se budu
muset poohlídnout po nějaké kvalitní náhradě.

A možná bych si mohl udělat radost a na léto si koupit nějakou kvalitní
běžeckou větrovku. Ale to opravdu jenom pro radost --- moje stará bunda
z _Tchiba_ už mě podržela na několika zimních a podzimních horských
závodech v délce 21-30 km. Takže nová bunda není v případě špatného
počasí nutnost.

### Boty

V botách mám jasno --- letos jsem si koupil nový trailovky _Inov-8
TerraUltra G 260_ a jsou to zatím ty nejlepší boty na (suchý) trail,
co jsem kdy měl.

Zatím jsem v nich neběžel žádný závod, ale cekově jsem v nich naběhal
přes 300 km, šest běhů v kopcích v délce 15-21 km a (opět celkově)
jsem v nich nastoupal přes 8.000 výškových metrů.

Takže na tyhle boty se můžu spolehnout.

{{< figure src="/2020/img/shoes/Inov-8-TerraUltra-G-260.jpg"
    link="/2020/img/shoes/Inov-8-TerraUltra-G-260.jpg"
    caption="Inov-8 TerraUltra G 260" >}}

Ještě je samozřejmě ve hře mokrá varianta, kdyby bylo hodně bláta.
Pak bych volil _Inov-8 X-Talon 230_. V těch už jsem běžel dva trailové
půlmaratony a dokonce jeden 30km [skyrace](//en.wikipedia.org/wiki/Skyrunning).

{{< figure src="/2020/img/shoes/Inov-8-X-Talon-230-Jested.jpg"
    link="/2020/img/shoes/Inov-8-X-Talon-230-Jested.jpg"
    caption="Inov-8 X-Talon 230" >}}

### Běžecká vesta

Letos jsem se konečně dopracoval, že jsem si koupil běžeckou vestu.
Jsem zvyklý běhat do nějakých 25 km bez pití. Při delším běhu už
si musím plánovat, kde doplnit tekutiny. Tohle řeší vesta. Plus se
do ní dají přibalit další věci --- kromě vody plánuju hůlky a gely
(viz dále) a možná i tu novou větrovku.

Pořídil jsem si _Inov-8 Race Ultra Pro 5 Vest_. Nechtěl jsem batoh,
ale zase ne jen čistou vestu a tohle je dobrý kompromis. 5ka v názvu
znamená 5 l mini-batůžek, takže se vejde fólie, svačinka, větrovka
apod. Tudíž něco se tam vejde, ale chci to držet minimalisticky.

Zároveň jsou v ceně dvě 1/2 l soft-flask láhve a silikonový kelímek
(čím dál častěji povinná výbava na trailových závodech). Hůlky se dají
uchytit na mnoho způsobů a kapes je mraky, z toho dvě nepromokavé.

Vestu jsem si zatím vzal na pár patnáctek, dvacítek a sedí jako
ulitá --- nikde netlačí, nikde nedře, člověk o ní ani neví. Litr
vody mi zatím přijde tak akorát. Jsem s ní hodně spokojený.

{{< figure src="/2020/img/Inov-8-Race-Ultra-Vest.jpg"
    link="/2020/img/Inov-8-Race-Ultra-Vest.jpg"
    caption="Inov-8 Race Ultra Pro 5 Vest" >}}

### Hůlky

Na používání hůlek jsem se vždycky díval trochu zvrchu --- to je přece
něco, co používají důchodci na [nordic
walking](//cs.wikipedia.org/wiki/Seversk%C3%A1_ch%C5%AFze). A správnej
ultráš to vždycky uběhne po svých, žádný pomůcky...

Pak jsem ale změnil názor --- když jsem loni v září "běžel" [Černá Hora
SkyRace](//cernahora-skyrace.cz/) (30 km/1.800 m) tak bych za hůlky
dal cokoli, klidně království. Ty dlouhé táhlé mnohakilometrové
stoupání mi úplně zmasakrovaly nohy. A hůlkaři? Ty si to kolem
mne štrádovali jak na výletě a předbíhal mě jeden za druhým.

Takže jsem si řekl: jestli se chci věnovat tomuhle typu závodů
a eventuálně horským ultra, budu potřebovat hůlky. Zapátral jsem
na trhu, pročetl a shlédl recenze a nakonec jsem si stejně pořídil
to, co prezentovali na startu v Černé Hořě --- skládací hůlky _Leki
Micro Trail Pro_.

Čas od času s nimi vyrazím trénovat (pod heslem: Buď připraven!) a
myslím, že to půjde. Zatím mi přijde, že je důležitá vhodná volba
vzdálenosti, profilu a terénu --- člověk má s hůlkama tak nějak
tendenci míň běhat. Což, pokud jde o prudká a dlouhá stoupání, tak to
nijak nevadí. Tohle ale každopádně prověří až závod.

{{< figure src="/2020/img/Leki-Micro-Trail-Pro.jpg"
    link="/2020/img/Leki-Micro-Trail-Pro.jpg"
    caption="Leki Micro Trail Pro" >}}

## Trénink

Poté, co jsem koncem března opustil ve 2/3 svůj maratonský plán,
běhám jen tak na pocit (a moje kilometráž jde dolů). Spíš než na objemy
se soustředím na trailové běhání s důrazem na převýšení.

Nikdy jsem nebyl uživatelem [Mapy.cz](//mapy.cz), ale teď jsem ocenil
jejich plánovací nástroj. Naklikal jsem si několik tréninkových tras,
v rozmezí 12-35 km s tím, že převýšení by mělo být aspoň 500 m.

Tyhle trasy jsem si nahrál do hodinek a postupně je jednou týdně běhám.
Někdy s hůlkama, častěji s vestou, někdy jen tak úplně nalehko. Je to
hodně výživný a doufám, že se to zůročí.

{{< figure src="/2020/img/Garmin-courses.jpg"
    link="/2020/img/Garmin-courses.jpg"
    caption="Garmin Courses převedený z Mapy.cz" >}}

Jelikož je letos kvůli [koronaviru](//cs.wikipedia.org/wiki/Covid-19)
specifická situace --- nebyly žádné jarní závody a letní sezóna je
vachrlatá, rozhodl jsem se zařadit (jen tak na pohodu a vlastně
do počtu) tréninkový trailový maraton: [Český kros,
Kunžak](//ceskykros.cz/kunzak/).

Je to měsíc před mojí premiérovou padesátkou, je to trail, jsou to
objemy, bude to krásný!

## Související články

* [Běžecké cíle 2020](/2020/bezecke-cile-2020/)
* [Český Kros Kunžak, maraton Českou Kanadou](/2020/cesky-kros-kunzak-maraton-ceskou-kanadou/)
