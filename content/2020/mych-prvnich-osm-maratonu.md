+++
title = "Mých prvních osm maratonů"
date = 2020-07-03T13:00:56+02:00
tags = ["maraton", "závody"]
description = """Během uplynulých šesti let jsem bězel osm maratonů. Jaké byly?"""
thumbnail = "2020/img/marathons/Medals.jpg"
+++

{{< figure src="/2020/img/marathons/Medals.jpg"
    link="/2020/img/marathons/Medals.jpg" >}}

Jestli letos půjde všechno podle plánu, měl bych zaběhnout svůj devátý
a desátý maraton. A o těch nejspíš něco napíšu. Co ale těch zbylých
osm předešlých? Zapomínám čím dál víc a malá rekapitulace neuškodí.

## 1. Maraton Praha, 2014

Můj první maraton, který už se ztrácí v romantickém oparu nostalgie.
Trénoval jsem na něj 8 měsíců a naběhal cca 800 km. To pěkně z čisté
vody, kdy jsem v létě vstal z gauče s velmi slušnou nadváhou.

Silným zážitkem byl hromadný start --- melodie _Vltavy_ a 10.000 běžců,
to je slušná dávka emocionální energie, která vžene slzu do oka.

Dodnes si pamatuji, jak mě rodina vítala na trati. Doběh si moc neuvědomuji,
jen že jsem měl v hlavě prázdno, žádná euforie. Celý následující týden
mě fest bolely kvadricepsy, ale hekal jsem s určitou hrdostí. Týden na to
už jsem plánoval další maraton.

## 2. Kladenský maratón, 2014

Od svého druhého maratonu jsem si sliboval prolomení hranice 4 hodin. Nestalo
se. Bylo to celkem ambiciózní očekávání (oholit 1/2 hodiny není jen tak),
ale i tak jsem se zlepšil o 21 minut.

Startuje a finišuje se na místním stadiónu _Sletiště_ a běží se osm zhruba
5km okruhů přilehlým lesoparkem. Příjemný závod, s fajn organizací,
žádná masovka.

Trénoval jsem podobně jako na první maraton --- podle online plánu
na [Runkeeperu](//runkeeper.com/), dodržoval jsem jenom objemy,
tempa jsem ještě neřešil. A asi jeden z posledních závodů, který jsem
běžel bez srdečního monitoru.

{{< figure src="/2020/img/marathons/Kladno.jpg"
    link="/2020/img/marathons/Kladno.jpg" >}}

## 3. Marató de Barcelona, 2015

{{< figure class="floatright" src="/2020/img/marathons/Barcelona-logo.jpg"
    link="/2020/img/marathons/Barcelona-logo.jpg" width="200" >}}

Do třetice všecho dobrého. Můj první zahraniční maraton a na dlouhou
dobu taky ten nejúspěšnější.

Trénoval jsem podle adaptabilního [Endomondo](//www.endomondo.com/) plánu,
kdy už jsem dodržoval všechna tempa, intervaly, atd. Řekl bych, že mají
docela dobře pochycenou predikci výsledného času --- to co předpovídali
mně, to jsem pak i zaběhnul.

Během závodu jsem se byl 2x vyčůrat a to mě stálo 4hodinovou hranici,
která mi utekla o pouhých 17 sekund. Dodneška si pamatuju ten 2km
finiš od [Monumento a Colón](//en.wikipedia.org/wiki/Columbus_Monument,_Barcelona)
na úrovni moře až k [Plaça d'Espanya](//en.wikipedia.org/wiki/Pla%C3%A7a_d%27Espanya,_Barcelona)
--- táhlé, 5% stoupání, rovné jako přímka. Bojoval jsem jako lev, ale
pod čtyři jsem to nedal.

Je to asi jeden z mála maratonů, který jsem zaběhl v perfektní kondici
a kde jsem nezažil _maratonskou zeď_. Ukázková tepovka od začátku
do konce:

{{< figure src="/2020/img/marathons/Barcelona-HR.jpg"
    link="/2020/img/marathons/Barcelona-HR.jpg" >}}

Barcelona je krásné město a proběhnout ho skrz na skrz je zážitek. Pokud
se před váma (doslova) zničehonic objeví monumentální [Sagrada
Família](//cs.wikipedia.org/wiki/Sagrada_Fam%C3%ADlia), nebo
20metrová Miróova socha [Mujer y pájaro](//cs.wikipedia.org/wiki/%C5%BDena_a_pt%C3%A1k),
je to vzpomínka na celý život.

{{< figure src="/2020/img/marathons/Barcelona.jpg"
    link="/2020/img/marathons/Barcelona.jpg" >}}

## 4. Maratón Montevideo 42k, 2015

{{< figure class="floatright" src="/2020/img/marathons/Montevideo-logo.png"
    link="/2020/img/marathons/Montevideo-logo.png" width="200" >}}

Měsíc po _Barceloně_ jsem běžel svůj 4. maraton. Asi to byla chyba, ale
nemohl jsem odolat --- byl jsem zrovna pracovně v Uruguayi a zaběhnout si
při té příležitosti maraton je životní šance.

Dělal jsem si aspirace, že bych (rutinně) zaběhnul stejný čas jako
v _Barceloně_, ale to jsem se přepočítal. Byl to (a doposud pořád je) můj
nejtěžší maraton. Ne svými parametry, ale tím, v jakém stavu jsem byl já.

Jednak jsem asi nebyl ještě pořádně zregenrovaný po minulém závodě, jednak
jsem měl slušný 5hodinový [jet-lag](//cs.wikipedia.org/wiki/P%C3%A1smov%C3%A1_nemoc),
jednak bylo vedro (tuším takových 28 °) a organizace nebyla úplně excelentní
(inu, Latinská Amerika) --- na zpáteční cestě maratonu na spoustě občerstvovaček
úplně chyběla voda! Marně jsem volal: _"¡Agua, agua!"_ Byl jsem dehydrovaný a
po 30. km se mi stalo to, co na maratonu ještě nikdy... musel jsem občas
potupně přejít do chůze. Jediný, co mě přimělo to dokončit, byl slib dceři,
že přivezu medaili. Tak jsem to nevzdal.

{{< figure src="/2020/img/marathons/Montevideo-route.png"
    link="/2020/img/marathons/Montevideo-route.png" >}}

Nicméně, trasa byla příjemná --- vyjma úvodních a finálních 2-3 km se běželo
celou dobu po přímořské promenádě a i když _Montevideo_ v podstatě nemá
žádné pamětihodnosti, ani zajímavou architekturu, byl to fajn závod.

A večer jsem si dal pořádný uruguayský steak --- takový si v Evropě nedáte!

{{< figure src="/2020/img/marathons/Montevideo.jpg"
    link="/2020/img/marathons/Montevideo.jpg" >}}

## 5. Jesenický maraton, 2016

{{< figure class="floatright" src="/2020/img/marathons/Jesenik-2016.png"
    link="/2020/img/marathons/Jesenik-2016.png" width="200" >}}

Rok 2016 byl rokem, kdy se u mne začalo projevovat pracovní vyhoření. První
obětí byl květnový _Pražský maraton_ --- ač jsem měl zaplacené startovné,
tak jsem klouzavě dospěl k tomu, že ho nechci běžet, že na to fyzicky nemám.

Avšak v půlce roku jsem se přeci jen vzchopil a zkusil _Jesenický maraton_.
To byla pro mne velká neznámá. Určitě mi pomohlo, že jsem dva týdny předtím
strávil s rodinou dovolenou v Alpách, kde jsem si dal pár horských 25tek,
s pěkným převýšením. Každopádně, nevěděl jsem, co od horského maratonu
čekat.

Nakonec to byla krásná premiéra. Kromě úvodního 5km stoupání na _Šerák_ jsem
běžel na tepovku, abych nepřešvihl 142 BPM. Pak už to byla krásná hřebenovka,
proložená _Pradědem_ na 26 km, zakončená hutným 4km klesáním do cíle.

Hřebeny Jeseníků stojí za to si proběhnout a pro mne se z tohoto závodu
stala srdcová záležitost --- až na výjimky, jen málokdy běžím některý závod
podruhé... a tady to jistě bude potřetí a víc.

{{< figure src="/2020/img/marathons/Jesenik-2016-start.jpg"
    link="/2020/img/marathons/Jesenik-2016-start.jpg" >}}

## 6. Dresden Marathon, 2016

{{< figure class="floatright" src="/2020/img/marathons/Dresden.png"
    link="/2020/img/marathons/Dresden.png" width="200" >}}

Asi druhý nejtěžší maraton. V práci jsem procházel pracovním vyhořením,
což mělo velký vliv na fyzičku (třeba letitá klidová tepová frekvence
42 BPM se mi chronicky zvedla na 64 BPM). Po _Jesenickém maratonu_ jsem
sice měl dobrý pocit, ale týden, čtrnáct dní před _Drážďanama_ jsem se
cítil vyčerpaný.

Nicméně na startu jsem se cítil narvaný endorfinama a dokonce jsem se
rozhodnul běžet o 5 minut rychlejší čas. Já vím, neuhlídaný ambice. Ale
to už byla jen taková třešnička na dortu.

Z maratonu si vlastně nic nepamatuju, pouze jediný zlomový bod --- na metě
půlmaratonu jsem to cítil... došlo mi. A nebyl to jen pocit, všechny moje
maratonské zdi jsou věrně doloženy mojí tepovkou. Zbytek závodu bylo jen
20 kilometrů dlouhý trápení, podobné deliriu. Ale dokončil jsem. A nakonec
to ani nebyl můj nejhorší čas. Jsem rád za takovou zkušenost.

{{< figure src="/2020/img/marathons/Dresden-HR.jpg"
    link="/2020/img/marathons/Dresden-HR.jpg" >}}

## 7. Copenhagen Marathon, 2019

{{< figure class="floatright" src="/2020/img/marathons/Copenhagen-logo.jpg"
    link="/2020/img/marathons/Copenhagen-logo.jpg" width="200" >}}

Pracovní vyhoření si vzalo svou daň --- v podstatě jsem na rok a půl přestal
běhat. Znovu začít se mi podařilo až s nástupem do nové práce. Veškerá fyzička
byla pryč, začínal jsem se stejnou kondicí a se stejnými tempy, jako když jsem
trénoval na první maraton.

Trvalo mi celý rok, než jsme se psychicky a fyzicky cítil na to, běžet
další maraton. Ano, můj 6. a 7. maraton dělí dva a půl roku. Začal
jsem zvolna několika půlmaratony a pak jsem před _Kodaní_ začal trénova
podle své upravené [Hanson Marathon Method](//www.goodreads.com/book/show/18857731)
(podle níž jsem trénoval i na _Drážďany_).

_Kodaň_ je krásné město --- na kulturu, na ježdění na kole, na běhání.
Na start jsem si v pohodě dojel na [bikesharingovém](//cs.wikipedia.org/wiki/Syst%C3%A9m_sd%C3%ADlen%C3%AD_kol)
kole (a stejným způsobem jsem se po závodě dostal do hotelu). Závod samotný
šel jako po drátkách --- když má člověk poctivě natrénováno, tak to jde
samo. Sice jsem o 10 minut nedosáhl na čas, na který jsem trénoval, ale
s přehledem jsem poprvé prolomil hranici 4 hodin.

Součástí startovacího balíčku byly i různý vouchery do kodaňských restaurací,
takže jsem si k večeru užil parádní hamburger. (Mimochodem, jídlo je v Kodani
výborné všude.)

Krásný závod! Asi ten nejhezčí silniční maraton, co jsem zažil.

{{< figure src="/2020/img/marathons/Copenhagen.jpg"
    link="/2020/img/marathons/Copenhagen.jpg" >}}

## 8. Jesenický maraton, 2019

{{< figure class="floatright" src="/2020/img/marathons/Jesenik-2019.png"
    link="/2020/img/marathons/Jesenik-2019.png" width="200" >}}

Návrat na místo činu. Po _Kodani_ jsem si chtěl dát nějaký trailový maraton
a moc jsem nad tím nepřemýšlel --- jednak to vycházelo z hlediska plánování,
a druhak... dobré vzpomínky.

Na podruhé jsem měl v cíli o 16 minut horší čas. Že by chybějící běhání
v Alpách? Neměl jsem moc natrénované kopce a dlouhých tréninkových běhů
taky moc nebylo. Poprvé se mi stalo, že mě na maratonu chytly křeče.
Na občerstvovačce na [Jelení studánce](//www.jeseniky.net/index.php?obl=1&kat=11&sluz=81&pol=2815)
jsem se narval magnéziovýma tabletama (které tam naštěstí měli) a doklepal
to dokonce. Závěrečný 3km sestup byl lahůdka.

Ale i tak, opět krásný závod! Jeseníky jsou srdcovka a tenhle závod jsem
neběžel naposled. Že bych to příště zkusil s hůlkama? Úvodní trhák
na [Šerák-Keprník](//cs.wikipedia.org/wiki/%C5%A0er%C3%A1k-Keprn%C3%ADk)
dokáže pořádně vyždímat. Stejně jako výstup na [Švýcárnu](//cs.wikipedia.org/wiki/%C5%A0v%C3%BDc%C3%A1rna_(Kouty_nad_Desnou))
a na [Petrovy kameny](//cs.wikipedia.org/wiki/Petrovy_kameny). Uvidíme...

{{< figure src="/2020/img/marathons/Jesenik-2019-start.jpg"
    link="/2020/img/marathons/Jesenik-2019-start.jpg" >}}

## Osm maratonů

Tak to je příběh mých osmi maratonů. Je to příběh s otevřeným koncem
--- zítra běžím svůj devátý. A i ten jubilejní už mám letos naplánovaný.
Pomalu, postupně budou přibývat...

{{< figure src="/2020/img/marathons/8-marathons.png"
    link="/2020/img/marathons/8-marathons.png" >}}

## Související články

* [Moje maratonské vyznání](/2020/moje-maratonske-vyznani/)
