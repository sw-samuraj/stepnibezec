+++
title = "Moje běžecké boty"
date = 2020-04-02T23:00:22+02:00
tags = ["boty"]
description = """V každých botách naběhám 1.000 km a pak jdou do důchodu.
    Kolik takových páru už jsem proběhal?"""
thumbnail = "2020/img/My-shoes.png"
+++

{{< figure src="/2020/img/My-shoes.png" link="/2020/img/My-shoes.png" >}}

Mám takové pravidlo --- v běžeckých botách naběhám tisíc kilometrů a pak
je vyhodím a koupím si nové. Boty jsou po té době už obvykle dost potrhané,
podrážka ošlapaná, netlumí... zkrátka, nevypadají dobře. A nové boty mi
udělají radost a zároveň zvednou motivaci.

Od té doby, co jsem začal (znovu) běhat, jsem naběhal nějakých 8.000 kilometrů, 
takže podle rovnice už jsem pár párů spotřeboval. Jak už jsem si před časem
povzdechl, člověk zapomíná a ne všechno je ve statistikách (i když hodně mi
v tomhle pomáhá _Garmin Gears_, kde si použítí bot zaznamenávám).

Tohle jsou tedy boty, ve kterých jsem sbíral své zkušenosti a úspěchy:

## Bezejmenné Asics

{{< figure src="/2020/img/Asics-logo.png"
    link="/2020/img/Asics-logo.png"
    width="400" >}}

Moje první běžecké boty byly "bezejmenné" _Asicsy_ --- nebyly z žádné
pojmenované řady, měly jenom nějaké číslo a koupil jsem je v nějakém
sportovním výprodeji.

Podstatné bylo, že mi padly jak bačkůrky (dodnes si pamatuji ten
pocit). V nich jsem začel trénovat na svůj první maraton a taky jsem
ho v nich odběhnul ([Pražský maraton](//www.runczech.com/cs/) 2014).

Každopádně tyhle boty způsobily, že jsem značce _Asics_ zůstal nějaký
čas věrný.

## Asics Gel Cumulus 15, bílé

{{< figure src="/2020/img/shoes/Asics-Gel-Cumulus-15-white.jpg"
    link="/2020/img/shoes/Asics-Gel-Cumulus-15-white.jpg" >}}

První "opravdové" běžecké boty. Koupil jsem si je na maratonském
expu před svým prvním maratonem, trénoval v nich celou sezónu
a odběhnul v nich svůj druhý maraton ([Kladenský
maraton](//maratonkladno.cz/?cat=50) 2014), plus čtyři půlmaratony
(_Olomouc_ 2014, _Miřejovice_ 2014, _Hradec Králové_ 2014 a
_Zimní běh na Blaník_ 2015).

Hodně tlumený model, vhodný na objemové tréninky. Vzhledem ke své
váze v nich ale klidně můžu běhat i závody. Kromě gelového tlumení
si ještě vybavuju, že svršek moc nevydržel a taky měly dost
nestandardní číslování --- vycházelo to tak o 1/2 čísla jinak,
než u jiných značek a modelů.

## Asics Gel Cumulus 15, oranžové

{{< figure src="/2020/img/shoes/Asics-Gel-Cumulus-15-orange.jpg"
    link="/2020/img/shoes/Asics-Gel-Cumulus-15-orange.jpg" >}}

Stejný model jako předešlý, jen v jiné barvě --- ty minulé jsem úspěšně
vyreklamoval poté, co se mi po 1/2 roce svrškem proklubaly oba palce. 👎
Ale jinak se v nich běhalo dobře. 👍

V tomhle páru jsem běžel dva maratony: [Marató Barcelona](//www.zurichmaratobarcelona.es/eng/)
2015 a [Maratón de Montevideo](//montevideo.gub.uy/areas-tematicas/deportes/maraton-de-montevideo)
2015. A taky _Jihlavský půlmaraton_.

## Salomon Sense Pro

{{< figure src="/2020/img/shoes/Salomon-Sense-Pro.jpg"
    link="/2020/img/shoes/Salomon-Sense-Pro.jpg" >}}

Moje první trailovky, určené pro... city trail. 🤭 Opět jedna z těch bot,
co padnou jako ulité, plus šikovný [Quicklace](//www.salomon.com/en-us/blog/how-use-the-salomon-quicklace-system)
systém na utahování, namísto klasických tkaniček. Hodně mělký vzorek, ale
na suchý trail byly výborný.

Běžel jsem v nich sice jen jeden delší závod --- _KTRC Ještěd Skyrunning
půlmaraton_ 2016 --- ale zato jsem v nich naběhal hodně kopců.

## Inov-8 F-Lite 195

{{< figure src="/2020/img/shoes/Inov-8-F-Lite-195.jpg"
    link="/2020/img/shoes/Inov-8-F-Lite-195.jpg" >}}

Moje první "Inovejty". 🤗 Tyhle jsem si vlastně koupit nechtěl --- chtěl
jsem si koupit nějaké _Inov-8_, tak jsem šel do [Trailpointu](//www.trailpoint.cz/)
a prodavač mi tam nesmyslně doporučil tyhle (chtěl jsem "něco na maraton").

Jde o minimalistický model určený primárně pro [cross-training](//en.wikipedia.org/wiki/Cross-training)
(do posilovny), ale když s nima člověk natrénuje, jsou výborné na suchý
trail. Začátky v nich byly hodně těžký --- strašně mě bolela lýtka, ale
postupně jsem si zvykl a nakonec si je zamiloval.

Běžel jsem v nich několik půlmaratonů (_Karlovy Vary_ 2015, _Olomouc_ 2015),
přičemž _Zátopkův zlatý týden_ 2015 je doposud můj nejrychleší půlmaraton
vůbec. Taky jsem v nich dal _Horský běh Perštejn-Klínovec_ 2015.

A ačkoliv jsem si na začátku nedokázal představit, že bych v těhle šlupkách
uběhnul maraton, nakonec jsem v nich bez problémů absolvoval [Jesenický
maraton](//jesenickymaraton.cz/) 2016.

## Asics DS Trainer 20

{{< figure src="/2020/img/shoes/Asics-DS-Trainer-20.jpg"
    link="/2020/img/shoes/Asics-DS-Trainer-20.jpg" >}}

Na svůj příští silniční maraton jsem chtěl něco lehkého, ne moc tlumeného
a ne tak minimalistického jako _F-Lite_ a tak jsem si koupil tyhle
tempovky od _Asicsu_.

Odběhal jsem v nich téměř všechny intervaly ze svých nedávných maratonských
plánů, několik půlmaratonů (_Praha_ 2016, _Zátopkův zlatý týden_ 2016,
_Jihlavský půlmaraton_ 2016 a 2018) hlavně [Dresden Marathon](//www.dresden-marathon.com/)
2016.

Na krátké vzdálenosti jsou to výborné, svižné tempovky, bohužel, u delších
(maratonských) vzdáleností se u nich projeví, že jsou ve špičce úzké,
což mi nevyhovuje --- po _Drážďanech_ mě z nich hodně bolely prsty a
slezl mi jeden nehet. 👎 Ale nezanevřel jsem na ně, pro běhy mezi
10-20 km jsou pořád fajn.

## Vivobarefoot Primus Road M

{{< figure src="/2020/img/shoes/Vivobarefoot-Primus-Road-M.jpg"
    link="/2020/img/shoes/Vivobarefoot-Primus-Road-M.jpg" >}}

V roce 2017 jsem měl těžkou běžeckou krizi a tak jsem se rozhodl pro radikální
změnu --- koupil jsem si barefoot boty. Odběhal jsem v nich celé léto,
převážně kolem 6-8 km na běh a byla to vlastně jediná kilometráž
za daný rok.

Celkově jsem v nich zatím naběhal pouhých 130 km a žádný závod. Ale pořád
se chystám, že je víc využiju. Tak třeba letos, protože proběhnout se
v nich v lehkém, nekamenitém trailu je příjemné.

## Inov-8 RoadClaw 275

{{< figure src="/2020/img/shoes/Inov-8-RoadClaw-195.jpg"
    link="/2020/img/shoes/Inov-8-RoadClaw-195.jpg" >}}

Značku _Inov-8_ jsem si zamiloval a tak když začali vyrábět silniční model,
chtěl jsem ho vyzkoušet. Koupil jsem si druhou verzi a odběhal v nich
veškeré silnice za poslední dva roky.

Po běžecké krizi jsem moc závodů neběžel a tyhle boty mě provázely na dva
silniční závody: půlmaraton _Zátopkův zlatý týden_ 2018 a hlavně
[Copenhagen Marathon](//copenhagenmarathon.dk/en/) 2019.

## Inov-8 TrailTalon 235

{{< figure src="/2020/img/shoes/Inov-8-TrailTalon-235.jpg"
    link="/2020/img/shoes/Inov-8-TrailTalon-235.jpg" >}}

První pořádné trailovky od _Inov-8_. Když jsem je kupoval, ještě pořád
jsem byl v rauši minimalistických a méně tlumených bot a tak jsem sáhnul
po lehčí verzi 235. Dnes bych si spíš vzal tlumenější 290ky.

Nebyla to špatná bota, ale nijak nadšený jsem z ní nebyl. Nicméně,
výborná na suchý trail. Když je trochu mokro, tak už to na blátě
dost klouže.

Běžel jsem v nich [Klánovický půlmaraton](//www.klanovickypulmaraton.cz/)
2018, [Prague Park Race Prokopské údolí](http://pragueparkrace.cz/prokopske-udoli/)
2018, [Borecký půlmaraton](//boreckypulmaraton.webnode.cz/) 2019, [Prague
Park Race Průhonický park](//pragueparkrace.cz/pruhonicky-park/) 2019 a
[Hruboskalský půlmaraton](//hruboskalskypulmaraton.cz/) 2019.

Svou životní cestu pak zakončily královskou disciplínou --- dal jsem si
podruhý [Jesenický maraton](//jesenickymaraton.cz/) 2019.

## Inov-8 X-Talon 230

{{< figure src="/2020/img/shoes/Inov-8-X-Talon-230.jpg"
    link="/2020/img/shoes/Inov-8-X-Talon-230.jpg" >}}

S modelem _TrailTalon_ jsem byl na traily docela spokojený, ale když jsem se
přihlásil na [Ještěd Winter SkyRace](//jestedskyrace.cz/winterskyrace/), tak
jsem trochu zpanikařil a koupil si boty do těžkého trailu.

Zatím jsem v nich moc nenaběhal, jen něco přes 150 km, což je hlavně tím,
že běžně v tak těžkém trailu neběhám a většinou na ten trail musím
v Praze doběhnou pár kilometrů po silnici. Jakmile ale jsem v technickém
terénu, v blátě, nebo na sněhu, tak tyhle boty drží jako přibité. 👍

Kromě _Ještěd Winter SkyRace_ 2018 jsem v nich běžel také [Zimní běh na
Blaník](//www.behnablanik.cz/) 2019 a [Brdský kros](//krosovypohar.cz/brdsky-kros/)
2019. Opravdovou prověrkou ale byl náročný 30km závod [Černá Hora
SkyRace](//cernahora-skyrace.cz/) 2019, asi nejtěžší závod, co jsem kdy běžel
--- boty to daly parádně, moje stehna se z toho ale sbíraly pár týdnů. 😣 😬

## A to ještě není všechno!

Když na to tak koukám, tak za těch pár let je to slušná přehlídka bot.
A není kompletní --- jelikož jsem teď po naběhaných tisícovkách odložil
jak _TrailTalony_, tak _RoadClawy_, pořídil jsem si nové boty do trailu
i na silnici.

Určitě o nich ještě uslyšíme, ale jejich čas teprve přijde. Zatím je nebudu
zmiňovat, ale jako ochutnávku: na trail jsem si pořídil jeden z nových
_Inov-8_ s [grafenovou](//www.inov-8.com/row/graphene) podrážkou a na silnici
jsem si chtěl vyzkoušet nějaké balóny od [Hoka One One](//www.hokaoneone.eu/en/cz/home/).
Jakmile i jejich čas uplyne, asi o nich také něco napíšu.
