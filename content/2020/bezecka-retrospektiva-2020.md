+++
title = "Běžecká retrospektiva 2020"
date = 2020-12-31T20:16:44+01:00
tags = ["maraton", "trail", "ultra", "závody"]
description = """Jak vypadal můj běžecký rok 2020?"""
thumbnail = "2020/img/Retro-2020.jpg"
+++

{{< figure src="/2020/img/Retro-2020.jpg" link="/2020/img/Retro-2020.jpg" >}}

Letos jsem si na jaře udělal plán běžecké sezóny. [COVID-19](//cs.wikipedia.org/wiki/COVID-19)
už byl hezky rozběhnutý, tak bylo celkem jasné, že to nedopadne podle mých
(či jiných) představ. Co se z toho nakonec vyklubalo? Co vyšlo a nevyšlo?

_(Mimochodem, situace svádí k tomu se nějak ke covidu vyjádřit... Ale neudělám to.
Vlastně jsem víceméně přestal sledovat sociální síťě --- nechci to číst, jak
tihle jsou pro roušky a tamti proti a tihle se chovají jako debilové a tamti...
Škoda mluvit. 😢)_

## Maratony

Plánoval jsem klasicky dva maratony --- jeden jarní, jeden podzimní. Z těch,
na který jsem byl přihlášený nevyšel ani jeden. Pražský byl prvně odsunut
na podzim a následně zrušen úplně. O startovné jsem zatím nepřišel, bylo
automaticky přesunuto na rok 2021. Tak uvidíme, zatím stránky [RunCzech](//www.runczech.com/)
o nadcházející sezóně mlčí.

Abych o jarní maraton nepřišel úplně, absolvoval jsem v červenci závod
[Český kros, Kunžak](//ceskykros.cz/kunzak/) --- velmi příjemný závod
Českou Kanadou. Bral jsem to hlavně jako přípravu na nadcházející
_Krkonošskou padesátku_ (viz dále). Vzhledem k tomu, že jsem to bral
tréninkově a byl to trailový maraton, tak jsem rezignoval na nějaké
honění času. Hlavní bylo vyzkoušet běžeckou vestu a zaběhnout si
delší trasu.

Po tom, co se sezóna de facto rozpadla, jsem v létě začal řešit co
s podzimem a přihlásil jsem se na _Maraton Praha-Dobříš_ ze seriálu
[Trail Running Cup](//www.trailrunningcup.cz/). Měl jsem ho běžet už
loni (ale nezbyly mi síly) a tak jsem si chtěl dát reparát. Bohužel,
závod se posouval, prvně z října na listopad, pak na prosinec...
Nakonec proběhl jakýsi individuální hybrid, který byl oznámen dva dny
před startem. Hodně špatná komunikace! 👎

### Maratonské články

* [Český Kros Kunžak, maraton Českou Kanadou](/2020/cesky-kros-kunzak-maraton-ceskou-kanadou/)

## Ultra

Poprvé v životě jsem si střihnul vzdálenost delší, než maraton: [Krkonošskou
padesátku](//www.trailrunningcup.cz/sezona-2020/krkonose-maraton/). Zážitek
více méně pozitivní. Jediný, co bych vytknul --- málo trailu a občas slabší
organizace. Jinak vzdálenost mi sedla, užil jsem si to a rád bych si zkusil
něco delšího. Nějakou šedesátku, možná i 50-mílovku.

### Ultra články

* [Krkonošská padesátka, můj první (baby) ultra trail](/2020/krkonoska-padesatka/)

## Traily

Letos to bylo jednoznačně ve znamení trailu! Neběžel jsem jediný silniční
závod a většinu tréninků jsem absolvoval v terénu. Hodně jsem běhal podle
navigace v hodinkách, skvělá věc. 👍

Jediný zbývající závod, který jsem letos absolvoval spadá taky do téhle
kategorie: [Černá hora SkyRace](//cernahora-skyrace.cz/). Hezký, ale těžký
závod v masivu Černé hory. Běžel jsem ho podruhé a zlepšil jsem se. Z toho
mám radost.

Jako úplný závěr sezóny můžu brát nezávodní výběh na Sněžku, v délce nějakých
27 km. Proběhnout se v listopadových Krkonoších je zážitek. V tomhle typu
aktivit budu určitě pokračovat.

### Trailové články

* [Černá Hora SkyRace, co tě nezabije...](/2020/cerna-hora-skyrace/)
* [Sněžka 25k](/2020/snezka-25k/)

## Roční objem

{{< figure class="floatright" src="/2020/img/2k-goal.png"
    link="/2020/img/2k-goal.png" width="200" >}}

I letos jsem si naplánoval zaběhnout za rok 2.000 km. I letos,
tak jako v minulých letech, jsem to nesplnil. Zůstal jsem viset
na 1.526 km, to jest nějakých třech čtvrtinách. Nakonec se mi
nepodařilo zaběhnout ani sekundární cíl  1.000 mil (1.609 km).
Tak třeba příště.

Ale našlápnuto jsem docela měl, zejména na jaře (maratonský plán
dělá hodně). Pak se s mizejícími závody motivace dřít vytratila
a běhal jsem si jen tak pro radost. S tím klesaly objemy. A
koncem podzimu, začátkem zimy mě motivace běhat přešla úplně.

To nevadí. Příští rok si ten _2k_ plán dám znovu. Ono to jednou
vyjít musí. 🤞

{{< figure src="/2020/img/Year-mileage.jpg"
    link="/2020/img/Year-mileage.jpg" >}}

## Celkově

Celkově mám za letošní rok dobrý pocit --- běhalo mi to, výkonostně
jsem spokojený, přesun ze silnice na trail je dokonán. Je ve hvězdách,
co přinese příští rok. Ale opět to bude rok běhání!

## Související články

* [Běžecké cíle 2020](/2020/bezecke-cile-2020/)
