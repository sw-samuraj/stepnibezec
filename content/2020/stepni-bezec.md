+++
title = "Stepní běžec"
date = 2020-03-17T14:06:52+01:00
description = """Zakládám si běžecký blog. Bude to o maratonech
    a trailovém běhání."""
+++

Už se k tomu schylovalo nějaký čas --- chtěl jsem si vést nějaké běžecké
zápisky. A čím jsem starší, tím méně si pamatuji. 🤦‍♂️

Takže... bude to občasník o běhání. Proč _Stepní běžec_? Má to dva rozměry:
Jednak se už řadu let přesouvám ze silnicí na traily (což se mi celkem
daří). A i když si občas střihnu nějaký sky-běh, tak mým nejoblíběnějším
trailem zůstává mírně zvlněná krajina, kterou se vine single-trail.

A potom, jedna z (divokých) interpretací mého příjmení je právě...
[stepní běžec](//cs.wikipedia.org/wiki/Stepn%C3%AD_b%C4%9B%C5%BEci).
Nomen omen.
