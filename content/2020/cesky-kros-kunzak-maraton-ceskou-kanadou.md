+++
title = "Český Kros Kunžak, maraton Českou Kanadou"
date = 2020-07-09T18:40:27+02:00
tags = ["maraton", "trail", "závody", "vysočina"]
description = """Český Kros, Kunžak je trailový maraton a půlmaraton
    v oblasti České Kanady. Můj devátý maraton."""
thumbnail = "2020/img/kunzak/Shoes.jpg"
+++

{{< figure src="/2020/img/kunzak/Shoes.jpg"
    link="/2020/img/kunzak/Shoes.jpg" >}}

Loni jsem měl takový plán --- právě jsem uběhl svůj osmý maraton a říkal
jsem si, že by bylo pěkný, zaběhnout si na ten desátý stejný závod, jako
na ten první. Tedy, _Pražský maraton_. Přidal jsem na podzim ještě jeden
maraton jako "výplňový" a přihlásil se na _Prahu 2020_.

Člověk míní, pánbůh mění. Vypadl mi jak ten devátý podzimní (z osobních
fyzických důvodů), tak z kvůli [koroně](//cs.wikipedia.org/wiki/Covid-19)
posunuli (a nakonec úplně zrušili) i ten pražský. Ovšem v mezičase, než
k tomu došlo, jsem přemýšlel, jak to pořád udělat, aby ten _Pražský
maraton_ byl pořád ten jubilejní.

A protože to dobře časově vycházelo, vybral jsem si letos jako svůj devátý
maraton [Český Kros, Kunžak](//ceskykros.cz/kunzak/). Závod, který má
v podtitulu "srdcem [České Kanady](//cs.wikipedia.org/wiki/%C4%8Cesk%C3%A1_Kanada)".

{{< figure src="/2020/img/kunzak/Trkal.jpg"
    link="/2020/img/kunzak/Trkal.jpg"
    caption="Výhled poblíž viklanu Trkal (https://mapy.cz/s/lakosaheba)" >}}

Jelikož situace kolem závodů je v době post-koronavirové často nejistá,
byl jsem nejistý i já --- na startovce maratonu bylo přihlášených 16 běžců
--- takže jsem si pro jistotu 2x ověřil, že se závod opravdu koná. Byly to
planné obavy --- závod je sice organizačně one-man-show _Daniela Bláhy_,
ale zvládnutý byl výborně: od značení, přes občerstvení, po medaili.
A samozřejmě čipová časomíra. 👍

Startovalo se komorně od místního rybníka [Komorníka](//mapy.cz/s/legabunega),
z přilehlého kempu. Nespornou výhodou bylo, že si člověk mohl dát po závodě
pivko/birell a rovnou se vykoupat v rybníku.

Na start maratonu nás nakonec vyráželo 12, z toho jen jediná žena. Pole se
hned po startu roztrhalo a až na jednu opakovanou výjimku, jsem celou
trasu běžel sám a sám. To mně nijak nevadí, ba naopak.

Trasa maratonu byla dvoukolová --- dva půlmaratonské okruhy. Dlužno říct,
že půlmaratonci startovali hodinu po nás a tak prakticky nemohlo dojít
k tomu, že by se závodníci pomíchali. Tady by to při těch počtech nijak
nevadilo, ale i tak, musím ocenit promyšlenou organizaci.

Onou výjimkou, která mne vytrhla z mojí běžecké samoty, je ona již zmíněná
slečna, se kterou jsme se v každém kole předbíhali --- prvně ona mne, pak
zas já ji. Není to galantní, ale musím říct, že na konec zkušenosti
zvítězily nad mládím. 🤭 Nestává se mi to často.

{{< figure src="/2020/img/kunzak/Skalka.jpg"
    link="/2020/img/kunzak/Skalka.jpg"
    caption="Skalka, 703 m (https://mapy.cz/s/pekuzubalu)">}}

Moje běžecká strategie byla jasná --- už od výběru závodu jsem to plánoval
jako přípravný trénink na [Krkonošskou padesátku](//www.trailrunningcup.cz/sezona-2020/krkonose-maraton/),
takže jsem to šel bez tréninkového plánu, bez ambicí a na pohodu. Řídil jsem
se jen dýcháním a pocitem při běhu.

Tahle strategie mi vyšla dobře, ale jen o běhání to nebylo. Počítal jsem
s tím, že bude teplo (až vedro) a tak jsem si vzal svou běžeckou vestu,
abych měl na trase dost vody. (A taky, abych na ni byl pořádně zvyklý.)

To byl dobrý tah, protože trasa byla tvořená jak dlouhými lesními úseky,
tak obdobně dlouhými přeběhy po loukách. V lese příjemně, na lukách
vedro. Takže na každém okruhu jsem bez problému vypil litr nesené vody.

Další novinkou, kterou jsem si vyzkoušel, byl běh na gely. Prozatím jsem
každý maraton běžel jen na ovoce z občerstvovaček. Tady jsem si vzal s sebou
čtyři gely (_GU Energy Gel, slaný karamel_), které jsem si dával na každém
devátém kilometru. Nějaký nášup energie jsem necítil, ale ani žádný
propad... takže to asi fungovalo.

Jinak občerstvovačky v tomhle závodě byly dobře nastavený --- na maratonské
trati jich bylo pět (cca po 7 km), měly vodu a ionťák, banány, hroznový víno
a broskve. 👍 A ještě si člověk příjemně pokecal s dobrovolníky.

{{< figure src="/2020/img/kunzak/Start.jpg"
    link="/2020/img/kunzak/Start.jpg" >}}

Do cíle jsem nakonec doběhnul v čase 4:53, jako sedmý muž (i celkově), hodinu
a půl za vítězem. To se mi ještě nestalo, že bych byl v TOP 10. 🤦‍♂️  Moje
_Garminy_ mi naměřily délku trasy 41 km, takže kilometřík tam ještě chyběl,
ale což... stejně to nebyl osobák, tak co. Nastoupané převýšení bylo hezkých
655 m, což při trailovém "tréninku" potěší.

V cíli jsem se odstrojil, plácnul sebou do rybníka a vychutnal si zaloužený
birell. Velká spokojenost. Jen si říkám, že příští maraton už to taková
pohoda nebude.

{{< figure src="/2020/img/kunzak/Elevation.jpg"
    link="/2020/img/kunzak/Elevation.jpg" >}}

## Související články

* [Mých prvních osm maratonů](/2020/mych-prvnich-osm-maratonu/)
* [Moje maratonské vyznání](/2020/moje-maratonske-vyznani/)
* [Běžecké cíle 2020](/2020/bezecke-cile-2020/)
