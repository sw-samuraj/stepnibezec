+++
title = "Krkonošská padesátka, můj první (baby) ultra trail"
date = 2020-08-07T11:02:19+02:00
tags = ["ultra", "trail", "závody", "krkonoše"]
description = """Zaběhl jsem si své první ultra - sice maličké, ale přece
    jenom. MTRC Krkonošká padesátka 2020."""
thumbnail = "2020/img/krkonose/Start.jpg"
+++

{{< figure src="/2020/img/krkonose/Start.jpg"
    link="/2020/img/krkonose/Start.jpg" >}}

Tak je to tady! Mám za sebou svůj první ultra trail. Sice jen takový maličký,
pouhou padesátku (a zasloužilí ultráši by se mi nejspíš smáli), ale technicky
je to tak --- 50 km je víc, než maraton. Takže ultra! 💪 😂

## Proč Krkonošská padesátka?

Závod [Krkonošská padesátka](//www.trailrunningcup.cz/sezona-2020/krkonose-maraton/)
jsem si vybral kvůli pár důvodům:

* Je to trail v horách (aby ne, když je součástí série [Trail Running
Cup](//www.trailrunningcup.cz/sezona-2020/krkonose-maraton/)),
* je to 50 km, takže jen o 8 km víc, než maraton (který rutinně zvládám),
* profilem a převýšením to není tak daleko od [Jesenického maratonu](//jesenickymaraton.cz/)
  (který jsem už 2x běžel)
* a časově mi to dobře vycházelo mezi jarní a podzimní maraton.

## Příprava

Podrobněji jsem se o své přípravě na tenhle závod rozepsal v článku
[Příprava na první ultratrail](/2020/priprava-na-prvni-ultratrail/). Tady jen
shrnu pár bodů:

* Nijak systematicky jsem netrénoval, spíš jen pocitově běhal objemy ve (svém)
  středním tempu.
* S tím, jak jsem opustil svůj jarní maratonský plán (přesunutý a zrušený _Pražský
  maraton_) začaly klesat moje měsíční objemy --- z 230 km měsíčně jsem spadnul
  na nějakých 130-150 km.
* Celkem pravidelně jsem jednou týdne běhal trailové kopce, cca 15-20 km a
  400-800 m převýšení.
* Poté, co mi odpadnul jarní maraton, jsem si vzal jeden do počtu (ať neztratím
  rytmus) --- vybral jsem si [Český Kros Kunžak](//ceskykros.cz/kunzak/) a pojal
  ho jako trénink na _padesátku_.

## Vybavení

{{< figure src="/2020/img/krkonose/Running-kit.jpg"
    link="/2020/img/krkonose/Running-kit.jpg" >}}

Jelikož na závod není žádná povinná výbava (nabitý mobil stejně nikdo
nekontroloval), bylo mi jasné, že budu potřebovat jen dvě věci: běžeckou vestu
a trailové hole.

### Běžecká vesta

Nejdůležitější funkcí vesty je... že může nést vodu. Když běhám do 20 km, tak
můžu jít úplně bez vody, v podstatě při jakékoli teplotě. I 30 km v tréninku
zvládnu bez pití, pokud není víc jak 20 °C. Ale samozřejmě, když pití sebou mám,
je to hned radostnější.

Moje vesta (_Inov-8 Race Ultra Pro 5 Vest_) má dva 0,5 l soft-flasky. Což na 50 km
není moc, ale samozřejmě se dají na občerstvovačkách doplňovat. I bylo dost
studánek po trase. Odhadem jsem během závodu vypil takových 5 l tekutin, převážně
vody, něco ionťáku, trošičku Kofoly a na poslední občerstvovačce trochu nealko piva.

Ale vesta není jenom nosič vody. Dá se do ní dát mobil, větrovka, gely a občerstvení
na cestu atd. Takže za mne, na závody (a tréninkové běhy) 30+ je vesta nutnost.

### Trailové hole

Že je dobré mít na obdobný závod hůlky, jsem zjistil skrze tvrdou zkušenost ---
loňský [Černá Hora SkyRace](://cernahora-skyrace.cz/) mě vyškolil, že pokud nechci
mít po 20 km v horách rozsekaný stehna a nechci z kopce cupitat jako gejša, 🎎
jsou hůlky silně potřeba.

V závodě se mi to potvrdilo --- závěr _padesátky_ byl dlouhý 10km sestup. Celou
dobu jsem běžel, nikdo nepředběhl mne, zato já jsem předběhl zhruba 13 lidí.
Jedno běli společné... nikdo z nich neměl hůlky a já jsem je předbíhal přibližně
dvojnásobnou rychlostí. Uměl jsem si dobře představit, čím si procházejí. 😣

Hůlky, které jsem tímto závodem pokřtil, jsou skládací _Leki Micro Trail Pro_.
Pořídil jsem si je v květnu a průběžně jsem s nima chodil trénovat (zejména výše
zmíněné trailové kopce), abych si na ně zvykl.

{{< figure src="/2020/img/krkonose/Stoh.jpg"
    link="/2020/img/krkonose/Stoh.jpg"
    caption="Seběh ze Stohu, pohled na Luční horu" >}}

### Boty

Běžel jsem ve svojich oblíbených _Inov-8 TerraUltra G 260_, ale vzhledem k tomu,
že bylo pěkné počasí a trail byl suchý, byla nejpodstatnější vlastností boty
pohodlnost a to, že nezpůsobuje otlaky a puchýře. Vzorek byl nedůležitý ---
občas jsem viděl, že lidi běží v silničkách a nebyl s tím žádný problém.

### Občerstvení

Jako zdroj energie na cestu jsem si vzal 4 gely _GU Energy Gel_ s příchutí
slaný karamel. 🤗 Asi by se hodil jeden, dva navíc, ale bylo to všechno, co
jsem doma měl. Takhle jsem si dával jeden gel každých 11 km. Opět, podobně
jako v [minulém závodu](/2020/cesky-kros-kunzak-maraton-ceskou-kanadou/),
žádný nárůst energie jsem necítil, ale taky jsem během těch 50 km nezažil
jedinou krizi.

### Větrovka

Spíš tak tréninkově jsem si do vesty přibalil super-novou větrovku _Inov-8
Windshell_. Ani ne, že bych se bál počasí --- i když jsem si říkal, že nahoře
by mohlo foukat --- jako spíš, abych si zvykal nosit nějakou (budoucí) povinnou
výbavu. Nicméně, tahle větrovka je lehká jako pírko, takže jsem na ni okamžitě
zapomněl a vzpomněl jsem si na ni až druhý den, kdy jsem vestu vybaloval
do pračky. 🤭

{{< figure src="/2020/img/krkonose/Drevarska-cesta.jpg"
    link="/2020/img/krkonose/Drevarska-cesta.jpg"
    caption="Dřevařská cesta" >}}

## Průběh závodu

Závod startoval z lyžařského resortu _Svatý Petr_. Nikam jsem se nehnal a
postavil se na úplný konec startovního pole. Vybíhalo se, jak jinak,
do sjezdovky, byť do té nejmírnější, a pokračovalo se neustále 9 km do kopce,
prvně na _Hromovku_, přes _Boudy na Pláni_ až k nejvyššímu body trasy (můj
Garmin naměřil nějakých 1.298 nm.), poblíž _Stohu_ a pak zpátky dolů, dlouhým
4km traverzem do _Svatého Petra_.

Tu první kopcovitou část jsem šel, poměrně ostrým tempem, s hůlkama. Stejně,
jako všichni v mojí výkonostní kategorii. Šetřil jsem se, nevěda, co mě ještě
čeká. Dlouhý seběh jsem si užil --- seběhy (i ty technické) jsou mojí silnou
stránkou a dlouhé horské seběhy mi vyhovují. 💪 Jen je potřeba nevypadnout
ze soustředení.

Po obrátce ve _Svatém Petru_, kde byla teprve první občerstvovačka --- až
na 14. km(!) a bez jídla(!) --- jsme se pustili po malé odbočce po _Dřevařské
cestě_ do _údolí Bílého Labe_. Vlastně jsme oběhli _Kozí hřbety_. Pěkná pasáž,
ale opět, 9 km do kopce.

{{< figure src="/2020/img/krkonose/Elevation.jpg"
    link="/2020/img/krkonose/Elevation.jpg" >}}

To už jsme měli za sebou zhruba půlku závodu. Pak se kolem _Bílého Labe_
zbíhalo zpátky do _Špindlu_, který jsme ale jen lízli a někde na 35. km
začal závěrečný masakr --- výstup na _Medvědín_. Nejméně záživná část trasy
(po nekonečné silničce), byť s pěknými výhledy. 😣 Tady se oddělovalo zrno
od plev: kdo umí chodit kopce a kdo ne. Zde mi hůlky zatraceně pomohly.
Kdybych je neměl, tak na závěrečný úsek jsem jak zombie.

Poslední etapa --- cca 10 km --- byly rovinky a dlouhý, 6km seběh do cíle.
Tady došlo na lámání chleba. Kdo měl v předešlých kopcích hůlky, tak běžel
(rozumnou rychlostí). Kdo neměl, trpěl. Ačkoli jsem s nimi soucítil, předbíhal
jsem je nemilosrdně dvojnásobným tempem. 😈

Poslední cílový půlkilometr přes centrum _Špindlu_ byl úmorný. Byl už jsem
vyšťavený a sotva  jsem běžel. Ale hodně lidí fandilo a to člověka povzbudí.
Cílem jsem proběhl v čase 7:29 (oficiálně 7:30), pouhé 4 hodiny za vítězem
_Jirkou Čípou_. 🤭 Dcera mi pak řekla, že jsem v cíli vypadal _opravdu_
vyčerpaně. Měla pravdu.

{{< figure src="/2020/img/krkonose/Medvedin.jpg"
    link="/2020/img/krkonose/Medvedin.jpg"
    caption="Výstup na Medvědín" >}}

## Co se mi až tak nelíbilo

I když jsem byl se závodem spokojený, jsou dvě věci, které bych vytkl.
Za prvé, rozmístění občerstvovaček a informace o nich. První občerstvovačka
po startu byla až na 14. km, což mi přijde hodně. Navíc tam bylo jen pití,
ani jeden banán. Což vzhledem k tomu, že se probíhalo (znovu) startem,
beru jako oraganizační selhání. Další občerstvovačka pak byla hned na 18. km.
🤷‍♂️

Jednotlivé občerstvovačky měly (opakovaně) nevěrohodné informace o té
následující. Člověk se tak např. dozvěděl že: "další občerstvovačka je
až za 12 km" (byla za 7), či "další občerstvovačka je na 45. km" (byla
cca na 41.). Mimochodem, rozmístění občerstvovaček nesedělo ani
s propozicemi --- místo slibovaných 7 jich bylo jen 5.

Nicméně musím říct, že dobrovolníci na občerstvovačkách byli vždy
skvělí a vyjma té první, ošizené, byly k dispozici správné běžecké
ingredience: voda, ionťák, občas kofola, nealko pivo, banány, cukrové tablety,
sůl. Na té poslední měli dokonce domácí perník s čokoládovou polevou! 👍

Druhá věc, co mě trochu zklamala: že to nebylo až tolik trailový. Což
o to, po horách jsme poběhali dost, ale tak minimálně polovina trasy
byla po asfaltkách, panelech a zpevněných cestách. Na to, že se seriál
jmenuje _Trail Running Cup_ bych čekal, že tam bude víc lesních cest
a single trailů, jak to znám z jiných závodů.

Na druhou stranu, chápu, že naplánovat trasu, aby ji
[KRNAP](//cs.wikipedia.org/wiki/Krkono%C5%A1sk%C3%BD_n%C3%A1rodn%C3%AD_park)
schválil nebude úplně jednoduché. Ale přeci jen --- vzhledem k tomu,
že tenhle závod dynamicky mění trasu z roku na rok a že _padesátka_
má limit 300 lidí, určitě by šlo ji do budoucna položit více do trailu.

{{< figure src="/2020/img/krkonose/On-the-road.jpg"
    link="/2020/img/krkonose/On-the-road.jpg" >}}

## Desatero (ultra) začátečníka

Možná se někomu bude hodit pár zásad, jak pojmout své první ultra. Není to nic
kanonickýho, ale za sebe bych doporučil:

1. Vyzkoušený boty, ve kterých uběhnu bez puchýřů a otlaků minimálně maraton.
1. Vyzkoušený ponožky, ve kterých uběhnu bez puchýřů a otlaků minimálně maraton.
1. Vyrazit rozumným tempem, nikam se nehnat. Nenechat se strhnout --- zrno od plev
   se oddělí v poslední třetině, čtvrtině.
1. Kopce (nahoru) se chodí. Nejlíp s hůlkama.
1. Rovinky a kopce dolů běhat.
1. Pokud je terén moc technický, raději přejít do chůze, nebo opatrně zpomalit
   --- zranění kvůli chvilkové nepozornosti se nevyplatí.
1. Mít dostatek vody, znát rozložení občerstvovaček, či studánky po trase.
1. Mít dostatek občerstvení (gely, tyčinky).
1. Mít nouzových 50 Kč (v Krkonoších spíš 100-200) na neplánované občerstvení
   v boudě, u stánku, apod.
1. Myslet jen na nejbližší cíle (občerstvovačka, kopec, zatáčka, atd.), držet
   rytmus (raději zpomalit, ale nezastavovat).

## Poslední rada na závěr

Tuhle radu si vypůjčím od britského ultra běžce _Damiana Halla_
([@Ultra_Damo](//twitter.com/Ultra_Damo)), kterou jsem našel v jeho článku
[Mental tips for ultramarathons](//www.inov-8.com/blog/ultramarathon-mental-tricks/)
a kterou se snažím na závodech dodržovat:

> Usmívejte se na ostatní běžce, poděkujte dobrovolníkům, plácněte si s dětma.

{{< figure src="/2020/img/krkonose/Shoes.jpg"
    link="/2020/img/krkonose/Shoes.jpg" >}}

## Související články

* [Příprava na první ultratrail](/2020/priprava-na-prvni-ultratrail/)
* [Český Kros Kunžak, maraton Českou Kanadou](/2020/cesky-kros-kunzak-maraton-ceskou-kanadou/)
* [Běžecké cíle 2020](/2020/bezecke-cile-2020/)
