+++
title = "Běžecké cíle 2020"
date = 2020-03-19T14:06:53+01:00
tags = ["maraton", "trail", "ultra", "závody"]
description = """Vždycky si na běžeckou sezónu dělám plán: kolik
    maratonů, kolik horských závodů, jaké budou celkové objemy?"""
thumbnail = "2020/img/Running-silhouette.jpg"
+++

{{< figure src="/2020/img/Running-silhouette.jpg" link="/2020/img/Running-silhouette.jpg" >}}

Běžecká sezóna už dávno začala (jsem teď v půlce svého maratonského
plánu), ale závody jsou zatím v nedohlednu --- uvidíme, jak s tím
zamíchá právě probíhající epidemie [COVID-19](//cs.wikipedia.org/wiki/COVID-19).

## Maratony

I tento rok bych si rád zaběhnul dva maratony. Jako jarní jsem si chtěl dát
[ten pražský](//www.runczech.com/cs/akce/volkswagen-maratonsky-vikend-2020/index.shtml).
Po delším čase masovku a také jako návrat na místo činu. Zatím ho nezrušili a tak dál
poctivě trénuji. (A možná ten plán dotrénuju, i když ten maraton posunou/zruší. 🤔)

Druhý maraton plánuju klasicky na podzim, asi říjen (možná i listopad) a zatím
jsem si žádný nevybral. Ani jestli to bude trail (hory), nebo silnice. Spíš
podle situace:

* jestli na jarním zaběhnu slušný čas, bude to trail,
* pokud se to úplně nepodaří, asi zkusím ještě jednou silnici.

Tím se dostávám k času: trénuju a chtěl bych dosáhnout na **3:40**. 🤞

## Ultra

Už léta pokukuju po ultramaratonech, čtu si o nich a sním o nich. Letos jsem si
řekl, že bych to velmi, velmi opatrně zkusil. Vybral jsem si [Krkonošskou
padesátku](//www.trailrunningcup.cz/sezona-2020/krkonose-maraton/). Je to takové
"baby-ultra", ale každý nějak začínal. Profilem a převýšením se to hodně
podobá [Jesenickému maratonu](//jesenickymaraton.cz/), takže to bych mohl
rozumně zvládnout. Čas si netroufám odhadovat, ale po dvojí zkušenosti
z Jeseníku, bych řekl, že pod 6 hodin to nebude a do 7 bych to mohl doklepat.

## Traily

Vloni jsem měl po podzimním [Černá hora SkyRace](//cernahora-skyrace.cz/)
těžkou běžeckou krizi (asi jsem se na tom odvařil) a říkal jsem si, že se
na nějaké závody můžu vykašlat. Že si dám jeden-dva maratony a jinak šlus.

Pak mi trochu otrnulo, tak jsem se před vánoci motivoval, že bych si střihnul
[Krosový pohár](//krosovypohar.cz/) --- sérii krátkých 15tek v okolí Prahy.
Loni jsem z toho běžel jenom _Brdy_, letos jsem se přihlásil rovnou na celou
sérii.

## Roční objem

{{< figure src="/2020/img/Past-goals.png" link="/2020/img/Past-goals.png" >}}

Už pár let mám v ročních cílech zaběhnout objem 2 000 km. Zatím se mi to
ještě nikdy nepodařilo --- většinou jsem naběhal kolem 1.4k+ a většinou mi to
nabourala nějaká nemoc.

Uvidím, jak se to bude vyvíjet letos. Zatím mám splněnou téměř čtvrtinu a
jsem na dobré cestě. A tak jsem zvědavý na prosinec.
