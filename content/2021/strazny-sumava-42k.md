+++
title = "Strážný, Šumava 42k"
date = 2021-08-22T13:22:02+02:00
tags = ["trail", "šumava"]
description = """Proběhnul jsem si kousek Šumavy. Nevyšlo to úplně
    podle mých představ."""
thumbnail = "2021/img/strazny/Alpska-vyhlidka.jpg"
+++

{{< figure src="/2021/img/strazny/Alpska-vyhlidka-header.jpg"
    link="/2021/img/strazny/Alpska-vyhlidka-header.jpg" >}}

Vyrazili jsme s rodinou premiérově na kola na Šumavu a co čert nechtěl
--- děti omarodily střevní chřipkou, která se jich držela pár dní.
Pobyt už byl zaplacený, zrušit se nedal, tak jsme jeli s tím, že
děti budou odpočívat na pokoji a sledovat olympiádu. (Děti se nakonec
po dvou dnech zvetily a nějaký ty kola jsme přece jen najezdili.)

Tím pádem se mi uvolnil čas na delší výběh, což se mi na dovolené moc
nestává. Bydleli jsme ve Strážným, odkud jsem si naplánoval 40km okruh.
Šumavu moc neznám, nikdy jsem tady neběhal, tak jsem moc nevěděl, co
čekat --- jedna věc je naplánovat si trasu podle [Mapy.cz](//mapy.cz)
a druhá si to pak opravdu proběhnout.

Na těch 40 km mi vyšlo jen nějakých 1.000 m převýšení, takže jsem si
řekl, že to dám bez hůlek. Sice na závěr jsem si klasicky "naplánoval"
krpál, kde by se hůlky hodily, ale suma sumárum, Šumava mi přijde
hodně běhatelná.

Běžel jsem hned druhý den po příjezdu, takže nám ještě zbyly nějaké
rohlíky se salámem a žena prej, ať si jeden vezmu na cestu. Říkám si:
proč ne --- jeden pořádný rohlík je jako dva energetický gely, ne?
Tak říkám vám rovnou --- pokud vás někdy něco podobnýho napadne, tak ne,
ta rovnice neplatí. Rohlík prostě za dva gely nevydá.

{{< figure src="/2021/img/strazny/Knizeci-plane.jpg"
    link="/2021/img/strazny/Knizeci-plane.jpg"
    caption="Knížecí pláně" >}}

Kromě gelů a rohlíku jsem si na cestu vzal 3 l vody (bidon a dva
softflasky) a to bylo všechno. Kvůli rodinné konstelaci jsem si
nemohl vzít mobilní telefon, což bylo trochu riziko. Ale že bych kvůli
tomu zůstal sedět doma, to ne. Kdo se bojí, nesmí do lesa.

Vyrazil jsem ze Strážného po červené značce směrem na Knížecí pláně.
Prvních 6-7 km po cyklostezce nebylo moc záživných --- lesní asfaltka,
hustý smrkový les, furt do kopce, nic, na čem by oko spočinulo.

Jakmile se ale cyklostezka u Josefova oddělila a já jsem pokračoval
cestou pouze pro pěší, začala se mi krajina líbit --- lesní stezka
přes kořeny, chodníčky přes mokřady a malebné šumavské louky.
Zpětně hodnoceno, byla to ta nejhezčí část trati. 🤩

Na Knížecích pláních jsem zkouknul zbytky bývalého kostela sv. Jana
Křtitele a stočil se na sever po žluté směrem na Borovou Ladu. Cesta
lesem byla fajn, ale kousek před Ladou jsem se napojil na silnici,
což už tak fajn nebylo.

{{< figure src="/2021/img/strazny/Alpska-vyhlidka.jpg"
    link="/2021/img/strazny/Alpska-vyhlidka.jpg"
    caption="Alpská vyhlídka" >}}

Možná je to daný tým, že Šumavu moc neznám, ale na můj vkus je tady
příliš mnoho asfaltu a málo lesních stezek. Na kolo dobrý, ale na (moje)
běhání nic moc.

To už jsem se blížil polovině trasy, měl jsme v sobě jeden gel a dvě
třetiny rohlíku. A začínal jsem cítit únavu. Začínalo delší stoupání
vedoucí na Alpskou vyhlídku a nejvyšší body trasy --- Světlou a Obří
horu --- a já jsem jasně cítil, že potřebuju další gel.

Alpská vyhlídka určitě stojí za navštěvu --- krásný výhled na východní
Šumavu a za dobré viditelnost jsou údajně vydět i ony Alpy. Za vyhlídkou
jsem se konečně odpoutal od silnice a přes kopce skrze lesní cestu po červené,
zamířil do Kubovy Hutě. Druhá nejkrásnější část cesty.

Za Kubovou Hutí, pod Boubínským hřebenem, vedou klikaté slatě, kde člověk
musí dávat pozor, aby neuklouzl --- čím větší únava, tím větší nárok
na pozornost. Jakmile se ale seběhne z prken dolů, běží se fajn.

{{< figure src="/2021/img/strazny/Shoes.jpg"
    link="/2021/img/strazny/Shoes.jpg"
    caption="Inov-8 TerraUltra G 270" >}}

Horní Vltavice odstartovala závěrečnou etapu --- nebyl bych to já, kdybych
si tam jako bonus nepřihodil kopec. Tady jsem se na sebe mrzel, protože
vystartovat do sjezdovy bez hůlek... 🤦‍♂️

Držel jsem se žluté značky a vytrvale stoupal do kopce. Moc nadšení ve mmě
nezbývalo --- energetické manko už jsem gely nedohnal a jelikož jsem
s sebou neměl telefon, nemohl jsem si zavolat odvoz. 😣

Ale člověk má v sobě ještě vždycky dost sil, aby dokončil --- jakmile
se cesta přehoupla přes kopec, bylo už jen otázkou zhruba dvou kilometrů,
než jsem si mohl dát zasloužené pivko. 🍺

Suma sumárum, Šumava mě (běžecky) nijak neoslnila --- jsou zde krásná místa, a
jsou zde dlouhé nudné úseky. Na můj vkus příliš mnoho asfaltu a málo trailu.
Ale třeba je to jenom mojí neznalostí. Třeba mi časem někdo ukáže pravé
šumavské trailové perly. 🤷‍♂️

## GPS trasa 

* [Trasa na Mapy.cz](//mapy.cz/s/baponaseru)

{{< figure src="/2021/img/strazny/Map.png"
    link="/2021/img/strazny/Map.png"
    caption="Mapa trasy" >}}

{{< figure src="/2021/img/strazny/Elevation.png"
    link="/2021/img/strazny/Elevation.png"
    caption="Profil převýšení" >}}

## Související články

* [Sněžné jámy, Kozí hřbety 30k](/2021/snezne-jamy-kozi-hrbety-30k/)
* [Sněžka 25k](/2020/snezka-25k/)
