+++
title = "Běžecké cíle 2021"
date = 2021-02-13T18:57:06+01:00
tags = ["maraton", "trail", "ultra", "závody"]
description = """Letos je sezóna nejistá, nedá se s ničím počítat.
    Ale i tak - co bych chtěl běžecky zažít v letošním roce?
    Na co se připravit?"""
thumbnail = "2021/img/Wolf.jpg"
+++

{{< figure src="/2021/img/Wolf.jpg" link="/2021/img/Wolf.jpg" >}}

Přijde mi, že letos bude běžecká sezóna ještě víc nejistá, než ta
podivná loňská. Lituju pořadatele závodů a chápu, že hledají alternativní
cesty, jak pokračovat. Já ale ty virtuální a individuální závody běhat
nebudu --- to si radši najdu úplně novou trasu a naplánuju si v mapách
svoji vlastní výzvu.

Věřím, že alespoň koncem jara, přes léto a snad i začátkem podzimu
budou klasické závody, tak jak je známe. Asi to budou většinou trailové
záležitosti, ale sem tam se najde přes léto i pěkný silniční půlmaraton
(pomrkávám na vás, [Miřejovice](//www.mkmirejovice.cz/trat) 😉).

## Ultra

Oproti loňsku jsem trochu prohodil pořadí sekcí a důvod je jasný ---
letos bude ultra vrcholem sezóny. Vloni jsem si vyzkoušel svůj první
[baby-ultra](/2020/krkonoska-padesatka/) a letos bych chtěl nějaký
plnohodnotný.

Kdyby byla normální sezóna, asi bych šel do něčeho 60k+, nebo 50mílového
(tj. 80k). Ale vzhledem k nejistotě čas přeje především [FKT](//fastestknowntime.com).
A tak jsem se rozhodl, že si dám [PVLH24](//www.pvlh24.cz/). Už po tomhle okruhu pokukuju
od loňského léta a teď přes zimu jsem se napevno rozhodl. Takže si dám
stovečku. 💪

## Maratony

Můj závazek --- dva maratony ročně --- asi letos nevyjde. Z loňska jsem pořád
přihlášenej na _Pražský maraton_, ale u toho nepočítám, že by byl na jaře
a kdoví, jak to bude s podzimem. První půlku roku chci stejně věnovat přípravě
na _PVLH_, pak se budu z _PVLH_ nějaký čas zbírat, takže to vypadá jen na jeden
podzimní maraton.

Spíš bych čekal, že na podzim to bude nějaký trailový maraton, protože na silniční
bych potřeboval natrénovat aspoň dva měsíce a pochybuju, že bych byl schopný
začít maratonský plán o prázdninách.

Zatím se mi jeví nejpřitažlivěji částečně čerstvá novinka ze seriálu
[Trail Running Cup](//www.trailrunningcup.cz/) --- v září by se měl běžet
[Králický maraton](//www.trailrunningcup.cz/zavody/kralicky-maraton), 42km
okruh přes _Králický Sněžník_, s převýšením 1.560 m.

Ale uvídíme --- kdyby třeba _Pražský maraton_ vyšel až na konec října, taky bych
ho zvážil.

## Traily

Traily budou určitě mému běhání dominovat. Budu pokračovat
v průzkumných bězích s navigací v hodinkách, jenom (v rámci přípravy
na _PVLH_) určitě prodloužím trailové trasy --- jestliže loni
jsem běhal převážně do 20k, tak letos to bude 20 a víc.

Bohužel, letos se nebude konat [Černá hora SkyRace](//cernahora-skyrace.cz/),
to mě mrzí. Takže u trailových závodů, kratších jak maraton, mě zatím
nic moc neláká. Snad s výjimkou [Brdského
půlmaratonu](//www.trailrunningcup.cz/zavody/brdsky-pulmaraton).
Ještě uvidím. 🤷‍♂️

A pak mám ještě z loňska převedeno pár závodu z [Krosového poháru](//krosovypohar.cz/),
takže když už je mám, tak si je taky zaběhnu.

## Roční objem

I letos jsem si dal svůj tradiční cíl --- zaběhnout za rok 2.000 km. Ještě se mi
to nikdy nepodařilo. Ale jednou to určitě příjde.

{{< figure src="/2021/img/Past-goals.png" link="/2021/img/Past-goals.png" >}}

## Související články

* [Běžecké cíle 2020](/2020/bezecke-cile-2020/)
