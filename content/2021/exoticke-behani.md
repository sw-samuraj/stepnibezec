+++
title = "Exotické běhání"
date = 2021-04-04T13:24:06+02:00
# tags = ["maraton", "trail", "ultra", "závody"]
description = """Náhodný tweet mě přiměl zavzpomínat na mé běhání
    v exotických destinacích."""
thumbnail = "2021/img/exotic/Formentor.jpg"
+++

{{< figure src="/2021/img/exotic/Formentor.jpg"
    link="/2021/img/exotic/Formentor.jpg" >}}

Náhodná zmínka na Twitteru mě uvrhla do víru vzpomínání --- kde všude
jsem běhal v zahraničí. Jednak jsem měl díky minulé práci možnost
procestovat kus světa, ale i dovolené různě po Evropě můžou přinést
krásné běžecké zážitky. Vezmu to chronologicky.

Co v tomto přehledu není, jsou zahraniční maratony, které jsem
popisoval v článku [Mých prvních osm maratonů](/2020/mych-prvnich-osm-maratonu/),
takže tady jenom pro úplnost vyjmenuju daná města: Barcelona, Montevideo,
Drážďany, Kodaň.

## Tsoutsouros, Kréta

{{< figure src="/2021/img/exotic/Tsoutsouros-map.png"
    link="/2021/img/exotic/Tsoutsouros-map.png" >}}

Můj první, zatím ještě neoficiální, půlmaraton. Byla to posezónní dovolená
na Krétě a už jsem byl přihlášený na svůj první maraton. Ještě nikdy jsem
dosud takovou vzdálenost neuběhnul. Navíc jsem vybíhal od moře do výšky
přes 500 m n.m. Ale nějak mi to prostě "běželo" a tak jsem dal přes deset
tam a stejnou cestou zpátky. Krétské hory jsou krásný a vysoký. Rád bych
se tam znovu vrátil.

{{< figure src="/2021/img/exotic/Tsoutsouros.jpg"
    link="/2021/img/exotic/Tsoutsouros.jpg" >}}

## Manila, Filipíny

{{< figure src="/2021/img/exotic/Manila-treadmill.png"
    link="/2021/img/exotic/Manila-treadmill.png" >}}

Přiznám se, tady trochu podvádím --- sice jsem v Manile (přesněji
v [Makati](//cs.wikipedia.org/wiki/Makati)) byl i jsem tam běhal.
Ale ne venku. 😳 V Manile bylo totiž takové horko, taková vlhkost
a takový smog, že jsem si neuměl běhání venku představit. A taky nás
důsledně varovali před kriminalitou. Tak jsem běhal v luxusním hotelu
na treadmillu. 🤷‍♂️

{{< figure src="/2021/img/exotic/Makati.jpg"
    link="/2021/img/exotic/Makati.jpg" >}}

## Gémenos, Provence

{{< figure src="/2021/img/exotic/Gemenos-map.png"
    link="/2021/img/exotic/Gemenos-map.png" >}}

[Provence](//cs.wikipedia.org/wiki/Provence) není jen kraj
[Cézanna](//cs.wikipedia.org/wiki/Paul_C%C3%A9zanne),
[van Gogha](//cs.wikipedia.org/wiki/Vincent_van_Gogh) a levandule.
Je také plná krásných vápencových kopců s epickými traily.
Jezdíval jsem tam služebně a ačkoliv nejsem ranní běžec, zde jsem
vybíhal do kopců za rozbřesku a ta procitající krajina má něco
do sebe.

{{< figure src="/2021/img/exotic/Gemenos.jpg"
    link="/2021/img/exotic/Gemenos.jpg" >}}

## Swansea, Wales

{{< figure src="/2021/img/exotic/Swansea-map.png"
    link="/2021/img/exotic/Swansea-map.png" >}}

Wales je svérázná země a i když bych si radši zaběhal
ve [Snowdonii](//cs.wikipedia.org/wiki/Snowdonia), osud mě zavál
do Swansea. Byl jsem tam pracovně, takže z města jsem se nikam
nedostal a tak jsem vzal zavděk plážovým během. Na konci pláže
je vesnička _Mumbles_ odkud pochází herečka [Catherine
Zeta-Jones](//www.csfd.cz/tvurce/606-catherine-zeta-jones/).

{{< figure src="/2021/img/exotic/Swansea.jpg"
    link="/2021/img/exotic/Swansea.jpg" >}}

## Montevideo, Uruguay

{{< figure src="/2021/img/exotic/Montevideo-map.png"
    link="/2021/img/exotic/Montevideo-map.png" >}}

V Uruguayi jsem byl pracovně a strávil jsem tam dohromady nějaký měsíc a půl.
Většinu času jsem byl v Montevideu a během té doby jsem hodně běhal ---
podle mých statistik jsem tam nasbíral 190 km. A to včetně dvou závodů:
_We Run MVD 10k_ a _Maratón Montevideo 42k_. Běhával jsem po přímořské
promenádě a dodnes mám na tu přívětivou zemi dobré vzpomínky.

{{< figure src="/2021/img/exotic/Montevideo.jpg"
    link="/2021/img/exotic/Montevideo.jpg" >}}

## Punta del Este, Uruguay

{{< figure src="/2021/img/exotic/Punta-del-Este-map.png"
    link="/2021/img/exotic/Punta-del-Este-map.png" >}}

A ještě jednou Uruguay. Na konci projektu jsme za odměnu strávili
víkend v _Puenta del Este_, tradičním uruguayském letovisku ---
je to něco jako Las Vegas u moře (kde prý údajně perou
v kasinech špinavé peníze jak z Argentiny, tak z Brazílie).
Nicméně, my jsme zde byli po sezóně a tak to spíš připomínlo
město duchů. Ale běhání po pláži bylo báječné. Podle Uruguayců
odděluje toto místo ústí [Río de la Plata](//cs.wikipedia.org/wiki/R%C3%ADo_de_la_Plata)
od Atlantického oceánu. To za mnou na obrázku je právě Atlantik.

{{< figure src="/2021/img/exotic/Punta-del-Este.jpg"
    link="/2021/img/exotic/Punta-del-Este.jpg" >}}

## Formentor, Mallorca

{{< figure src="/2021/img/exotic/Formentor-map.png"
    link="/2021/img/exotic/Formentor-map.png" >}}

Mallorca má mnohá kouzla, ale kdybych se tam měl ještě vrátit, určitě bych
volil severní hornatou část, jejíž asi nejdivočejší částí je mys
_Formentor_, s majákem na špičce. Nemohl jsem si tenkrát pomoci ---
i když bylo plné léto a teploty šplhaly ke 35° C, z pláže k majáku
a zpět to bylo hezkých 21 km a můžu vám garantovat, že jsem byl
jediný běžec na trase. Po doběhu jsem se v běžeckým plácnul
do moře a bylo mi báječně.

{{< figure src="/2021/img/exotic/Formentor-2.jpg"
    link="/2021/img/exotic/Formentor-2.jpg" >}}

## Lima, Peru

{{< figure src="/2021/img/exotic/Lima-map.png"
    link="/2021/img/exotic/Lima-map.png" >}}

V Peru jsem byl taky pracovně a i když jsem tam strávil celkově
skoro čtyři týdny, moc jsem toho nenaběhal --- vyjma teploty
a vlhkosti (v Limě je po celý rok kolem 20° C) byly podmínky 
dost podobné jako v Manile, tedy hlavně smog, kriminalita a obrovské
10milionové město s minimem parků. Zkusil jsem to dvakrát venku, ale
nestálo to za to --- uběhnutá vzdálenost tomu odpovídá.

{{< figure src="/2021/img/exotic/Lima.jpg"
    link="/2021/img/exotic/Lima.jpg" >}}

## Michaelerberg, Rakousko

{{< figure src="/2021/img/exotic/Michaelerberg-map.png"
    link="/2021/img/exotic/Michaelerberg-map.png" >}}

Běhání v Alpách má své kouzlo a zatím jsem ho, bohužel, zažil málo.
Na téhle dovolené jsem spojil příjemné s užitečným a využil běhání
v nadmořské výšce mezi 1.000-2.000 m n.m. jako ideální trénink
pro svůj premiérový horský maraton v Jeseníkách týden nato.
Od tý doby si říkám, že bych se do téhle hornaté krajiny kolem
Dachsteinu rád vrátil. I třeba na nějaký závod.

{{< figure src="/2021/img/exotic/Michaelerberg.jpg"
    link="/2021/img/exotic/Michaelerberg.jpg" >}}

## Londýn, Anglie

{{< figure src="/2021/img/exotic/London-map.png"
    link="/2021/img/exotic/London-map.png" >}}

V Londýně jsem byl služebně na konferenci a jelikož jsem byl zrovna
při síle, pořádně jsem si zaběhal. Londýn je veliký a tak se v něm
dají urazit slušné vzdálenosti. A pokud jste v centru, můžete oběhat
spoustu notoricky známých míst a pamětihodností.

{{< figure src="/2021/img/exotic/London.jpg"
    link="/2021/img/exotic/London.jpg" >}}

## Mønbroen, Dánsko

{{< figure src="/2021/img/exotic/Monbroen-map.png"
    link="/2021/img/exotic/Monbroen-map.png" >}}

Dánsko není země trailů a kopce jsou nedostatkové zboží. Pokud je naopak
někdo rovinkář, je to země zaslíbená. Není to destinace pro každého, musíte být
trochu scandi-pozitivní a spíš než na běhání je to krajina na kolo.
Takže kvůli běhání bych se do Dánska nevracel. Nicméně, jednu akci bych si
chtěl přece jenom zaběhnout --- [Hvide Sande Marathon](//www.beachmarathon.com/uk),
maraton v bílých píscích.

{{< figure src="/2021/img/exotic/Monbroen.jpg"
    link="/2021/img/exotic/Monbroen.jpg" >}}

## Utrecht, Holandsko

{{< figure src="/2021/img/exotic/Utrecht-map.png"
    link="/2021/img/exotic/Utrecht-map.png" >}}

Nejsem velký fanoušek městského běhání, ale když je člověk na služební
cestě a může se proběhnout kolem vody... nebo si oběhnout velký ostrov,
proč ne. 🤷‍♂️ Holandsko je běžecky zajímavý asi tak, jako Dánsko.
Ale maraton v Amsterdamu mám pořád ještě na seznamu (i když ne zas až
tak vysoko).

{{< figure src="/2021/img/exotic/Utrecht.jpg"
    link="/2021/img/exotic/Utrecht.jpg"
    caption="source: [Wikipedia.org](//nl.wikipedia.org/wiki/Amsterdam-Rijnkanaal)" >}}

## Premantura, Chorvatsko

{{< figure src="/2021/img/exotic/Premantura-map.png"
    link="/2021/img/exotic/Premantura-map.png" >}}

Premantura je na samé špici istrijského poloostrova a naštěstí je tam přírodní
rezervace, takže není zastavěná hnusnými hotely a bungalovy. Když si člověk
vezme na cestu dost pití, dá se v Chorvatsku běhat i v létě --- buď k večeru,
nebo ráno. A chorvatské vápencové pobřeží je pověstné svou rozervanou malebností.
Plus tady lze navíc najít stopy dinosaurů. 🦖

{{< figure src="/2021/img/exotic/Premantura.jpg"
    link="/2021/img/exotic/Premantura.jpg"
    caption="source: [Wikipedia.org](//hr.wikipedia.org/wiki/Rt_Kamenjak)" >}}

## Kde jsem si mohl zaběhat a nezaběhal

Je i pár míst, která jsem mohl přidat do svého běžeckého zápisníku, ale
nestalo se tak. Většinou kvůli pracovnímu vytížení, fyzickému či psychickému
vyčerpání, anebo proto, že jsem daný čas nechtěl obětovat běhání, ale věnoval
ho rodině.

Nejvíc mě asi mrzí, že jsem si nezaběhal v Norsku --- byl jsem čtyřikrát
v Oslu a okolní traily musí být super. 😢

Čistě jen jako bod pro odškrtnutí na seznamu mohlo být běhání v Africe ---
byl jsem pracovně týden v Akkře, hlavním městě Ghany. Bylo by to ale běhání
v nepěkném městě, v africkém vedru a hlavně jsem se bál malárie. 😱

Také jsem byl několikrát v Paříži 🗼 a ani jednou jsem neobul běžeckou botu.
Tak snad někdy --- i pařížský maraton je na mém dlouhodobém seznamu.

## Kde bych si chtěl zaběhat

Běhání v zahraničí je užasné --- rozmanité krajiny, které svět nabízí jsou
nepřeberné a každý si vybere podle svých kulturních, fyzických a estetických
preferencí. A tak, jak jsem jednou okusil jiné, než české trasy, sestavuji si
v duchu místa, kde bych si rád zaběhal. Některá se mi asi nikdy nesplní,
u některých je to jen otázka trpělivosti. Kam bych se tedy rád podíval?

* Maraton ve Stockholmu,
* maraton v Oslu,
* maraton v Tokiu,
* norské hory (kdekoliv --- sever, jih, cokoliv),
* opravdu vysoké Aply (Rakousko/Švýcarsko/Itálie),
* Slovinské Alpy,
* Pyreneje,
* Kanárské ostrovy, nebo Madeira.
* Irsko
  * Connemara
  * poloostrov Dingle
  * poloostrov Iveragh
  * Wicklow Mountains
* Lake District
* Skotsko
  * ostrov Jura
  * ostrov Skye
  * Cairgorms

Ten seznam není kompletní --- průběžně se mění a některé ikonické (a nejspíš
nereálné) věci na něm chybí. Ale něco z toho se mi určitě časem splní.
Poznáte to snadno --- bude to tady na blogu. 🤞

## Související články

* [Mých prvních osm maratonů](/2020/mych-prvnich-osm-maratonu/)
