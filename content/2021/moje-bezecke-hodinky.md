+++
title = "Moje běžecké hodinky"
date = 2021-06-20T21:53:01+02:00
tags = ["hodinky"]
description = """Za těch posledních 8 let, kdy běhám "trochu víc", jsem
    obměnil troje GPS hodinky. Takové malé ohlédnutí."""
thumbnail = "2021/img/garmin/Watches.png"
+++

{{< figure src="/2021/img/garmin/Watches.png"
    link="/2021/img/garmin/Watches.png" >}}

Je tomu osm let, co jsem se trochu víc rozběhal a jen o malinko
méně času uplynulo od nákupu mých prvních běžeckých hodinek.
Předtím jsem tři roky běhal jen tak na pohodu, pouze jsem si zapisoval
(z mapy odměřené) uběhnuté kilometry.

S užíváním prvního chytrého telefonu přišla fascinace statistikami,
které produkovaly tehdejší aplikace jako [Runtastic](//www.runtastic.com/)
(kterou jsem měl nejraději), [Runkeeper](//runkeeper.com/) (která byla
nejhorší), či dnes už zaniklé [Endomondo](//en.wikipedia.org/wiki/Endomondo)
(tu jsem měl druhou nejradši). Vyzkoušel jsem i leccos jiného, ale u téhle
trojky jsem pár měsíců zůstal.

A pak jsem se jednoho dne přihlásil na svůj první maraton a začal se chystat
na svůj první maratonský plán. A přestalo mě bavit běhat s mobilem v ruce.
Říkal jsem si, že když už jsem teď ten seriózní běžec, 😂 měl bych si
pořídit nějaké běžecké hodinky s GPS.

Dal jsem na radu zkušenějších a pořídil si své první Garminy. Své volby
nelituju, ale zároveň musím říct, že mi vzniknul [vendor lock-in](//en.wikipedia.org/wiki/Vendor_lock-in),
protože všechna svoje běžecká data teď mám v [Garmin Connect](//connect.garmin.com/modern/).
A tak, i když jsem občas mlsně pokukoval po jiných značkách hodinek
(hlavně Suunto 👀), vždycky jsem nakonec skončil u dalších Garminů. 🤷‍♂️

## Garmin Forerunner 110

{{< figure src="/2021/img/garmin/Forerunner-110.png"
    link="/2021/img/garmin/Forerunner-110.png"
    class="floatright" width="250" >}}

První běžecké hodinky jsem si pořídil v úsvitu své maratonské kariéry
a byly to ty nejlevnější na trhu. I tak, stály přes 3.000,- Kč a za tu
cenu mi přišly jako digitální plasťáky docela drahé.

Byl to ten nejnižší model, který Garmin v dané době prodával, takže
výběr funkcí byl velmi omezený --- v podstatě uměly jen dvě věci: měřit
vzdálenost pomocí GPS a spočítat aktuální a průměrné tempo.

Nicméně v té době mi to naprosto stačilo, protože hromadění uběhnutých
kilometrů bylo to jediné, co mě zajímalo. Dokonce i v rámci prvního
maratonského plánu jsem nic víc nepotřeboval --- dokonale jsem ignoroval
předepsaná tempa a držel se jen a jen rozepsané kilometráže.

Dokonce jsem ze začátku nechápal, proč všichni běžci řeší tempo (pace)
a proč hodinky místo toho neukazují rychlost v km/h. 🤦‍♂️

Každopádně, s hodinkama jsem se sžil, absolvoval v nich dva maratony
a po roce k nim přikoupil hrudní pás na měření srdečního tepu ---
zobrazovat srdeční tep naštěstí uměly.

Pak už jsem začal brát maratonské plány trochu vážně, a tak jsem začal
běhat tempa a intervaly. To první hodinky uměly. To druhé pro ně bylo
sci-fi, takže na intervalové tréninky jsem si brával jak hodinky, tak
telefon (Endomondo aplikace uměla intervaly naprogramovat zdarma) a
po doběhání jsem telefonní záznam z aplikace odmazával. 🙄

Tímhle stylem jsem natrénoval na další dva maratony a řekl si, že přišel
čas na kvalitativní a technologickou změnu.

## Garmin Forerunner 220

{{< figure src="/2021/img/garmin/Forerunner-220.png"
    link="/2021/img/garmin/Forerunner-220.png"
    class="floatright" width="250" >}}

Pro nákup novějších a lepších hodinek jsem měl jediný důvod --- zamiloval
jsem si běhání intervalů a chtěl jsem mít jejich trénink přímo v hodinkách.
Oproti minulému modelu jsem si připlatil další 2.000,- Kč, ale stálo to za to.

I designově vypadaly líp, měly lepší display, dokonce lehce barevný,
ovšem, pořád to byly plasťáky --- na jednu stranu dobrý, protože byly
lehký, na druhou stranu, ve společnosti s nima člověk velkou parádu neudělal
(pokud nechtěl okatě dávat najevo, že je běžec 🙄).

Tou dobou jsem pod vlivem četby [Hansons Marathon
Method](//www.goodreads.com/book/show/18857731) začal spřádat své vlastní
maratonské plány a zejména hansonovské intervaly (speed & strength) mi
učarovaly. Lehce jsem je modifikoval a všechny narval do hodinek.

Byl jsem spokojený --- odběhal jsem s nimi během pěti let spoustu závodů
(většinou půlmaratonů, ale i čtyři další maratony), trénoval tempa a intervaly
a pak... pak jsem si koupil kolo. 🤨

Problém (levnějších a středních) Forerunnerů je, že jsou jenom na běhání,
neumějí nastavit jiný typ sportu. Takže si sice změříte jízdu na kole,
ale na všechny ty sociální sportovní sítě se vám to nahraje jako běh.
Takže jsem to (opět) musel ručně editovat.

## Garmin Fenix 6 Pro

{{< figure src="/2021/img/garmin/Fenix-6-Pro.png"
    link="/2021/img/garmin/Fenix-6-Pro.png"
    class="floatright" width="250" >}}

Právě potřeba 🤭 multisportu mě (znovu) uvrhla do víru studování nabídky.
Vždycky jsem pokukoval po Suuntu, ale nakonec mě celá historie a archiv
mých běhů zase vrátila ke Garminu. 🤷‍♂️ Prvně jsem zvažoval vyšší
segment Forerunnerů 645 (zvyk je zvyk), ale pak jsem si řekl: udělám si
radost. A šel jsem do čerstvě uvedených Fénix 6.

Hlavním tahákem aktuální generace Garminů byly mapy a hudba. Na to první
jsem se těšil, to druhý (v hodinkách) totálně ignoruju. A Fénixy vypadají
fakt dobře. Cool! 😎

Hansonovské intervaly už jsem měl nachystaný, tak jsem je jenom synchronizoval
a začal trénovat na svůj první maraton s Fénixy. Žádné velké překvapení,
prostě to, co by člověk očekával. Jenže pak daný maraton zrušili a já
jsem přemýšlel, co s načatou běžeckou sezónou. 🤔

A tak jsem zvolna začal objevovat mapové funkce. A postupně jsem tomu
propadnul. Časem jsem si vypracoval následující postup:

1. Na [mapy.cz](//mapy.cz) zapíchnou prst do nejzelenější části nějaké
   oblasti a zazoomovat, aby byly vidět všechny stezky.
1. Naplánovat trasu přes pěkné kopce a údolí. Čím víc převýšení, tím lepší. 😈
1. Exportovat jako [GPX](//en.wikipedia.org/wiki/GPS_Exchange_Format) a nahrát
   do Garmin Courses.
1. Synchronizovat course do hodinek.
1. Jít běhat. 🏃‍♂️

Funguje to parádně --- už jsem takhle leccos proběhal v kopcích na západ
od Prahy, ale i jinde po Česku. S touhle kratochvílí souvisí i další vlastnost
a sice výdrž hodinek. Zatím jsem s Fénixy neabsolvoval nic delšího než padesátku,
ale počítám, že na "běžné" ultra to postačí. Prvním vážným testem bude
[PVLH24](//www.pvlh24.cz/), tak snad to klapne 🤞 Pro jistotu mám v záloze
maličkou powerbanku.

## Kolik těch hodinek člověk potřebuje?

Úplně chápu, pokud někdo ujíždí na sbírání hodinek. My běžci to známe ---
rád bych si vyzkoušel tyhle boty... a tyhle taky vypadají dobře. A stějně
tak s hodinkama, jen to bývá dražší sranda. Já se ale držím a taky mám jednu
výhodu --- mám rád, když starý věci dobře fungují (třeba pořád používám
10 let starý Kindle Touch).

Se současnými Fénixy jsem spokojený a věřím, že pár let s nimi ještě naběhám.
Občas dokonce přemýšlím, že bych běhal bez hodinek --- být zase trochu svobodný
--- ale zatím je láska ke statistikám silnější.

## Související články

* [Moje běžecké boty](/2020/moje-bezecke-boty/)
