+++
title = "Běhání na dýchání"
date = 2021-05-02T00:03:26+02:00
tags = ["trénink", "zkušenosti"]
description = """Rytmické dýchání při běhání, dechové vzorce, střídavý dech."""
thumbnail = "2021/img/Inhale-exhale.jpg"
+++

{{< figure src="/2021/img/Inhale-exhale.jpg" link="/2021/img/Inhale-exhale.jpg" >}}

Samozřejmě, při běhání dýcháme všichni. Bez dýchání to jaksi nejde. Nicméně
co se týká běžeckého dýchání, je tady jeden aspekt, který mě vyděluje
od většiny (mně známých) ostatních běžců.

Vždycky, když jsem se ptal těch zkušenějších, co běhají mnohem líp než já,
tak na mne koukali jak na exota. Vlastně jsem ještě nenarazil na nikoho,
kdo by to měl podobně jako já. A přitom v tom nemůžu být úplně sám ---
minimálně o tom [vyšla knížka](//www.goodreads.com/book/show/18892018-runner-s-world-running-on-air),
občas o tom píše [někdo na blogu](//www.balancedrunner.com/how-to-breathe-when-you-run-part-two/)...

O čem to mluvím? Jde o to, že běhání mám pevně spojené s určitými dechovými
vzorci. Nádech na určitý počet kroků, výdech na určitý počet kroků. Dělám
to úplně intuitivně a podvědomě. Někdy naopak vědomě ten vzorec změním, či
drobně upravím --- podle okolností, podle pocitů.

## Běhání je rytmus

{{< figure src="/2021/img/Bass-guitar.jpg" link="/2021/img/Bass-guitar.jpg" >}}

Běhání je pro mne rytmus. Ať už čistě mechanicky, desítky kilometrů
ve stejném tempu --- [even split](//en.wikipedia.org/wiki/Pacing_strategies_in_track_and_field#Even-split),
moje základní a jediná strategie, má síla i prokletí --- anebo, trochu
zvláštně, hudebně. Hudba je přece melodie a rytmus, ne? 🤷‍♂️

Matně si vzpomínám, že takhle jsem to měl už v dětství, kdy při tělocviku,
při běhání pověstné [12minutovky](//cs.wikipedia.org/wiki/Cooper%C5%AFv_test),
či smírnější [patnácti-stovky](//cs.wikipedia.org/wiki/B%C4%9Bh_na_1500_metr%C5%AF),
jsem dýchal s nějakým rytmickým vzorcem (jakým, to už si nepamatuji).

Někdy v 10 letech jsem začal chodit s maminkou na jógu a naučil jsem
se různá dechová cvičení (většinu z nich umím a dělám dodnes). Pak už nebylo
cesty zpět --- kdykoliv se mi "zvědomí" dýchání (což se mi u běhu děje
dost často), uvědomím si svůj dechový vzorec a případně ho podle potřeby
koriguju, hlavně podle námahy a terénu.

Další věc, která mi nerozlučně spojila běhání s rytmickým dýcháním
je hudba. Od dětství až do zhruba 30 let jsem byl aktivní muzikant. A dobrého
muzikanta poznáte mimo jiné podle toho, že "má rytmus". Spousta lidí si myslí,
že na hudbu je potřeba mít talent. Není to pravda --- tak jako u spousty jiných
věcí (třeba u sportu) se dají věci natrénovat (a pro úspěch je ta pravidelnost
důležitější, než jakýkoli počáteční talent). A já jsem rytmus trénoval opravdu
hodně.

Tahle hudební záležitost pro mne má ještě jednu rovinu --- nedokážu běhat
s puštěnou hudbou. Protože cítím rytmus v muzice a cítím rytmus při běhání,
jsou tyto dvě dvěci v nesmiřitelném nesouladu, který mne nesmírně irituje.
Vždycky je skladba v jiném tempu, nebo rytmu, než bych aktuálně potřeboval.

## Moje dechové vzorce

{{< figure src="/2021/img/Mosaic.jpg" link="/2021/img/Mosaic.jpg" >}}

Moje dechové vzorce se v průběhu života různě měnily --- dřív jsem je měl
kratší, rychlejší. Protože jsem běhal rychleji a protože jsem to jinak
neudýchal. Postupně s věkem a prodlužujícími se vzdálenostmi se začaly
prodlužovat i moje vzorce.

### Běžné objemy

Když běhám dnes takový ten klasický objemový běh, to jest zhruba 10k+
dýchám v 7/4 taktu --- nádech na 4 kroky, výdech na 3. To je pro mne
pohodové konverzační tempo. Není úplně jednoduché to přepočítat
na klasické běžecké tempo, protože to je výrazně ovlivněný
terénem. Ale podle rovinatosti-kopcovitosti bych to tempo viděl někde
nezi 5:30-6:30 minut na kilometr.

### Kopce a tempové běhy

Poslední rok a půl běhám v podstatě výhradně traily --- na silnici
jdu jenom výjimečně, když jsou moji běžečtí parťáci cimprlich se
za deště zablátit v terénu. 🤭 Anebo kdybych začal zase trénovat
na nějaký silniční maraton.

Do kopce se samozřejmě zvýší úsilí a dech se zkrátí. Takže běhám
v 5/4 taktu --- nádech na 3 kroky, výdech na 2. Totéž platí pro
tempové běhy. Tempový běh je pro mne tempo maratonu, nebo půlmaratonu.

Někdy, když je úsilí větší, než pro objemy, ale zase ne tak náročné
jako (ostřejší) tempo, používám kombinace 5/4 a 6/4 taktu: náchech
na 3 kroky, výdech na 3 kroky, nádech na 3 kroky, výdech na 2 kroky.
Tohle není úplně triviální --- jak pro běžce, tak pro muzikanty ---
ale chce to jen maličko cviku. 😈

V 5/4 tempu jsem taky například běžel svůj nejrychlejší silniční
maraton, od začátku do konce (s výjimkou finiše).

### Kratší závody, finiše a sky-runningové kopce

Je jasný, že tahle kategorie je nejnáročnější a nutně jenom krátkodobá.
Kratším závodem myslím v podstatě jenom 10k (kratší neběhám). Tady používám
3/4 takt --- 2 kroky nádech, 1 krok výdech. Totéž se mi děje u těch
opravdu prudkých kopců, jaké se dají potkat na sky-runningových závodech.
Jestli nevíte o čem mluvím, zkuste si představit výběh do sjezdovky. 🤦‍♂️

Co se týká (závodního) finiše, tam se to může lišit podle vzdálenosti
a strategie. U maratonu, půlmaratonu to vždycky závisí aktuálním stavu sil
--- někdy zrychlím tempo 2 kilometry před cílem, někdy nechám finish
na posledních 500 metrů. Každopádně, tenhle poslední úsek se více méně
blíží sprintu a tam se pak můžu dostat až na dýchání 1:1, co krok, to
nádech, nebo výdech.

## Běhat v lichém, nebo sudém taktu?

{{< figure src="/2021/img/Dice.jpg" link="/2021/img/Dice.jpg" >}}

Je jasné, že každému sedí trochu jiné tempo, běhá různé vzdálenosti,
má potřebu dýchat podle sebe. Je nějaké obecné pravidlo, které se
nevztahuje k výkonu a potřebě "to udýchat"?

Když se podíváte na moje vzorce, které jsem popsal výše, můžete si
všimnout, že jsou všechny liché. Nebylo tomu tak vždycky. Dřív jsem
běhaval na sudé vzorce. Například objemy jsem kdysi běhával
nádech na 4 kroky, výdech taky na 4 kroky. Závody jsem běhával 3
na 3.

Ono ve skutečnosti je pro lidi přirozenější držet a cítit sudý rytmus.
Nejvýraznější je to v muzice, ale platí to obecně. Proč tedy běhat a
dýchat na lichý rytmus? Kromě toho, že je to cool 😈 (v hudbě s tím
můžete dělat fakt hustý věci), má to u běhání jeden podstatný zdravotní,
či fyziologický aspekt.

Nevím, jestli jste nad tím někdy přemýšleli, ale pokud při běhu (chůzi)
dýcháte na sudý počet dob (třeba 2 kroky nádech, 2 kroky výdech),
došlapujete při nádechu (či výdechu) pořád na stejnou nohu. Naopak,
při lichém počtu dob došlapujete při výdechu střídavě na obě nohy. 

Proč je to důležité? Protože při výdechu se bránice a přilehlé svaly
uvolní, čímž je střed těla méně stabilní. Ve spojení s dopadovým stresem
při běhání a jednostranným monotónním zatěžováním (při výdechu pořád
na stejnou nohu) to může vést ke zranění.

Prý na to existuje studie. Nevím, jestli to tak je. Ale tahle teorie
mi přijde docela sympatická. A dokonce existuje studie, která spojuje
dýchání na sudý počet s pícháním v boku. Je pravda, že v mládí jsem
tím občas trpěl. A teď, poslední léta letoucí vůbec. 🤷‍♂️
Ale neberu to nijak dogmaticky, prostě mi spíš teď dýchání na lichý
rytmus vyhovuje.

## Je potřeba to pořád počítat?

Ne, není. To je na dýchání to zajímavé --- pokud ho nějak vědomě
"nastavíte", tak už na to nemusíte myslet. Možná to chce ze začátku
trochu cviku, stejně jako když se učíte hrát na hudební nástroj,
nebo meditovat. Po (krátkém) čase je to už tak automatický, že
to ani nevnímáte. Aspoň já to tak mám.
