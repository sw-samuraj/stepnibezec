+++
title = "FKT Černá Sněžka"
date = 2021-06-05T16:49:19+02:00
tags = ["fkt", "závody", "ultra", "trail", "vrcholy", "krkonoše"]
description = """Vymyslel jsem si FKT závod, tak jsem ho musel proběhnout."""
thumbnail = "2021/img/cerna-snezka/Snezka-z-Lisci.jpg"
+++

{{< figure src="/2021/img/cerna-snezka/Snezka-z-Lisci-header.jpg"
    link="/2021/img/cerna-snezka/Snezka-z-Lisci-header.jpg" >}}

Už si nevzpomínám na všechny peripetie toho nápadu, ale myslím, že to začalo
před dvěma lety, kdy jsem běžel první ročník závodu [Černá hora
SkyRace](//cernahora-skyrace.cz/) --- v jednu chvíli se přede mnou otevřelo
panoráma Krkonoš a uprostřed trůnila Sněžka. A říkal jsem si, že by bylo
pěkné si z Černé hory na Sněžku zaběhnout.

Ta idea se postupně vyvíjela, až to časem dospělo k okruhu, který jsem
nazval [FKT Černá Sněžka](//cernasnezka.cz). Ale o tom psát nechci ---
rád bych se podělil o to, jak jsem si tuhle štreku zaběhnul.

## Vybavení na cestu

Jedná se o okruh, který má podle [Mapy.cz](//mapy.cz/s/kuhumuresu) délku
51 km. Nasbírané převýšení se na jednotlivých mapových podkladech liší,
ale jde zhruba o 2.100-2.300 m. To už je slušná porce kilometrů a metrů,
navíc v horách, takže mít sebou rozumné vybavení je nutnost.

Ještě než začnu od oblečení, důležitá zmínka,
kterou zná každý, kdo chodí do hor --- sledovat počasí a nachystat se
na různé alternativy. Meteoslužby hlásily, že na Sněžce ten den bude něco
nad nulou, budou přeháňky a naštěstí bude jen velmi slabý vítr.

Takže jsem zvolil stejné oblečení, jako když jsme loni v půlce listopadu
běželi [Sněžka 25k](/2020/snezka-25k/): dlouhé slabé elasťáky, dvě trika
(s krátkým, s dlouhým) a větrovku. Pro jistotu jsem přibalil do batůžku
rukavice proti větru, běžkařskou čepici a náhradní triko.

Zbytek vybavení jsem měl téměř stejné jako na loňskou [Krkonošskou
padesátku](/2020/krkonoska-padesatka/) --- běžecká vesta, hůlky (kvůli
převýšení), litr vody v softflaskách, 6 energetických gelů, 2 raw tyčinky
a banán. 🍌

Jediná věc, kterou jsem měl oproti minule navíc, byla maličká power banka
a káblíky na napájení hodinek a telefonu. Ne, že bych to potřeboval ---
mám vyzkoušený, že obojí mi na tuhle vzdálenost vydrží --- ale jednak jsem
to měl pro jistotu a jednak jako trénink na delší vzdálenost.

{{< figure src="/2021/img/cerna-snezka/Vybava.jpg"
    link="/2021/img/cerna-snezka/Vybava.jpg"
    caption="Nachystaná výbava" >}}

## Logistika na trase

Takovouhle akci bez podpory jsem ještě neabsolvoval a tak je důležitý si
věci naplánovat --- už se mi párkrát stalo, že jsem při delším závodě či
tréninku něco podcenil a pak uprostřed lesů řešil dehydrataci, nebo křeče,
protože mi došlo kvůli nedostatku přísunu energie. 🤷‍♂️ Logistiku
na trase jsem si tedy rozdělil na tři části: navigace, pití a jídlo.

Nejjednodušší to bylo s jídlem --- spočítal jsem si gely (cca jeden
na 8-10 km), přidal něco navíc (tyčka, banán) a počítal, že si cestou
dám někde na příhodném místě polévku.

Navigace byla taky jednoduchá. Trasu jsem si nahrál do hodinek (běhám takhle
od loňského jara). Memorizace mapy taky trochu pomohla --- konec konců,
okruh jsem plánoval přes známá místa (jen se mi to ještě nespojilo dohromady).

Trochu obavy jsem měl z dostatku vody. Na loňské [letní
padesátce](/2020/krkonoska-padesatka/) jsem vypil 5 litrů tekutin. Pravda,
bylo léto a vedro, ale i tak --- aktuálně jsem plánoval nést jenom litr vody
a na trati nebyly žádné pořadatelem zajištěné občerstvovačky. Zkrátka
self-supported.

Prozkoumal jsem mapu, ale se studánkama to v Krkonoších není moc slavný.
Spoléhal jsem tak spíše na otevřené boudy a občerstvení --- podle mapy
jsem prošel všechna místa na trase, kde by se dalo koupit občerstvení
a podle jejich webových stránek se snažil zjistit, jestli v dnešní
epidemické době mají otevřeno.

Ponaučení je jasné --- pokud by byl úplný lockdown, byl by nutný
ještě nějaký camel back a nést si většinu vody na cestu sebou.

## Report z Černé Sněžky

{{< figure src="/2021/img/cerna-snezka/Cerny-dul.jpg"
    link="/2021/img/cerna-snezka/Cerny-dul.jpg"
    caption="Úbočím Černé hory, v pozadí vápenka v Černém dole" >}}

Jelikož jsem se svým nadcházejícím počinem nijak netajil, rozhodli se ke mně
přidat dva běžečtí parťáci, se kterými běhám ve všední dny. Jelikož jsou
v digitálním prostoru stydliví, budeme jim tarantinovsky říkat Mr. Fox 🦊
& Mr. Bear 🐻

Na startovní bod, centrální parkoviště v Jánských Lázních, jsme dorazili
lehce po 9. ráno a po převlečení a nachystání výbavy jsme v 9:35 vyrazili
na trasu. Při svých chabých zkušenostech jsem odhadoval, že na cestě
strávíme něco přes osm hodin.

Na okruh jsme se vydali po směru hodinových ručiček, tedy z Jánských
na severozápad, úbočím Černé hory, směrem na Liščí horu. Jde o táhlé
5km stoupání, které ničím moc nezaujme a kde jedinou odměnou je pohled
na vápenku v Černém dole.

{{< figure src="/2021/img/cerna-snezka/Snezka-z-Lisci.jpg"
    link="/2021/img/cerna-snezka/Snezka-z-Lisci.jpg"
    caption="Pohled na Sněžku z Liščí hory" >}}

Mr. Bear 🐻 & Mr. Fox 🦊 jsou lepší běžci než jsem já a tak, i když jsem
jim opakovaně říkal, že na těhle závodech, s těmihle parametry se kopce
(v mojí výkonnostní kategorii) chodí, ničeho nedbali a běželi a běželi.
Co mi zbývalo? Tak jsem taky do kopce běžel.

Po krátkém seběhu do údolí říčky Čistá nás čekal výběh na Liščí horu
--- další táhlý 5km výstup, tentokrát už s hezkými panoramatickými
výhledy. Na Liščí hoře, což byl zhruba 12. km, nás potkal první sníh,
většinou ve formě sněhových jazyků přes cestu. Taky jsme tady potkali
smíšenou dvojici běžců v protisměru.

{{< figure src="/2021/img/cerna-snezka/Lucni-Studnicni.jpg"
    link="/2021/img/cerna-snezka/Lucni-Studnicni.jpg"
    caption="U Zeleného potoka. V popředí Výrovka, v pozadí Luční a Studniční hora" >}}

Z Liščí hory je to téměř až pod Sněžku více méně rovinka (na trailové poměry),
krásně běhatelný úsek po krkonošské náhorní planině mířící mezi Luční a
Studniční horu.

Právě mezi těmito dvěma plochými horami byl kritický úsek, kvůli kterému jsme
náš pokus postupně o měsíc odkládali. Nicméně dnes už byla cesta nějaké dva tři
týdny vyfrézovaná a ani dál, od Památníku obětem hor až ke Sněžce už nebyl nikde
na cestě sníh.

{{< figure src="/2021/img/cerna-snezka/Snih-na-Lucni.jpg"
    link="/2021/img/cerna-snezka/Snih-na-Lucni.jpg"
    caption="Vyfrézovaný sníh na cestě mezi Luční a Studniční horou" >}}

Na Luční boudě si moji parťáci doplnili vodu a pokračovali jsme "zkratkou"
přes Polsko. Jestliže doposud jsme měli hory sami pro sebe, tak od Luční
začal provoz houstnout.

A zahustil se nejenom počet turistů, ale i počasí --- začalo trochu víc
profukovat, takže jsem nasadil kapuci u větrovky a sem tam nás zkrášlil
májový deštíček.

{{< figure src="/2021/img/cerna-snezka/Snezka-z-Polska.jpg"
    link="/2021/img/cerna-snezka/Snezka-z-Polska.jpg"
    caption="Pohled na Sněžku z polské strany" >}}

Samotnou Sněžku jsme vzali ztečí po řetězech ze strany od Obřího dolu. Že nám
to Sněžka nedá zadarmo, nám dala najevo skrze mlhu a déšť krupičkového sněhu,
který nás zasypával cestou vzhůru.

Mr. Fox 🦊 & Mr. Bear 🐻 měli jako vždycky náskok a tak když jsem je nahoře
dohnal, byli už trochu promrzlí. Dorazit sem nám trvalo nějakých 3 a 1/2 hodiny
a byli jsme na 22. km. Zaranžovali jsme vrcholové foto a bez dalšího
zdržování jsme pokračovali do příjemnějších partií hor.

{{< figure src="/2021/img/cerna-snezka/Studnicni.jpg"
    link="/2021/img/cerna-snezka/Studnicni.jpg"
    caption="Studniční hora (pohled od Sněžky)" >}}

Následující hřebenovka na Pomezní boudy je sice malebná, ale k běhání moc
není --- kamenné chodníčky vyžadují neustálé soustředění a není to nic
příjemného ani na kotníky, ani na unavené svaly. Ale výhledy jsou odtud
nádherné.

Postupně jsme se propracovali až do blízkosti Pomezních bud a tady jsme
poprvé (a naposled) seběhli z trasy --- navigace v hodinkách mi to neukázala
úplně přesně a tak jsme místo po červené pokračovali dál po Hřebenovce.
Dostali jsme se až na Tabuli (Skalny Stół) a dál nás to stejně kvůli
hnízdění tetřeva nepustilo. Přidali jsme si tak ke trase cca 1,5 km
a 100 výškových metrů.

{{< figure src="/2021/img/cerna-snezka/Snezka-summit.jpg"
    link="/2021/img/cerna-snezka/Snezka-summit.jpg"
    caption="Na vrcholu Sněžky" >}}

Na Pomezních boudách jsme si dali v pivovaru Trautenberk první a jedinou
zastávku. Objednali jsme si polívku kulajdu a birell, doplnili vodu a pokračovali
--- byli jsme na 30. km, takže už nás čekal jen půlmaraton a závěrečná lahůdka. 😈

Počasí se zlepšilo, vykouklo sluníčko, takže jsme sundali větrovky a běželi
lehce zvlněnou krajinou, nejdříve Pomezního hřebenu a poté Dlouhého hřebenu.
To jméno sedí --- je opravdu dlouhý.

{{< figure src="/2021/img/cerna-snezka/Tabule.jpg"
    link="/2021/img/cerna-snezka/Tabule.jpg"
    caption="Tabule (Skalny Stół)" >}}

Tady už únava začínala být znát a kopce jsme čím dál víc chodili. A i když
Mr. Bear 🐻 & Mr. Fox 🦊 pořád drželi čelní pozice, bylo vidět, že délka
trasy se na nás víc a víc projevuje.

Ke konci Dlouhého hřebenu nás čekal 3km seběh do Temného Dolu a alespoň sám
za sebe jsem cítil, že svaly na nohách ještě drží --- sice unavené, ale
ještě ne rozsekané. Asi za to mohly hůlky.

{{< figure src="/2021/img/cerna-snezka/Horni-Mala-Upa.jpg"
    link="/2021/img/cerna-snezka/Horni-Mala-Upa.jpg"
    caption="Horní Malá Úpa, pohled na Sněžku" >}}

Přímo v Temném Dolu, u kaple sv. Anny jsme narazili na moderní křížovou cestu.
Moje umělecké srdce zaplesalo a musel jsem ji celou zvěčnit.

Tou dobou už jsme za sebou měli přes 41 km a čekal nás největší mazec ---
výstup na Černou horu. Také nám už docházela voda a tak jsme já a Mr. Fox 🦊
naplnili láhve z přilehlého potoka Honzova ručeje. Jsme přece drsný chlapi
a vody z potoka se nebojíme!

{{< figure src="/2021/img/cerna-snezka/Krizova-cesta.jpg"
    link="/2021/img/cerna-snezka/Krizova-cesta.jpg"
    caption="Křížová cesta Temný důl" >}}

Závěrečné stoupání na Černou horu bylo zkouškou charakteru a mělo příjemné
sky-runningové parametry: délka 7 km, převýšení 670 m. Bylo to dlouhé, bylo
to úmorné a... nekonečné.

Tady došlo na lámání chleba --- Mr. Bear 🐻 nám po 2 km ve svahu řekl,
že už to není zábava, že se to nedá běhat a že se vrátí seběhem do Jánských
Lázní. Já a Mr. Fox 🦊 jsme pokračovali zbývajících 5 km na vrchol Černé
hory.

{{< figure src="/2021/img/cerna-snezka/Cerna-hora-summit.jpg"
    link="/2021/img/cerna-snezka/Cerna-hora-summit.jpg"
    caption="Na vrcholu Černé hory" >}}

Ale všechno zlé jednou končí --- nahoře jsme si udělali druhou vrcholovou
fotku a dokonce jsme zvládli i běžet rovinky a z kopce. Už to ale nebyl ten
křepký krok z počátku našeho dobrodružství --- naše běžecká forma se změnila
v klasické ultrášské cupitání.

Závěrečný seběh po sáňkařské dráze, necelých 5 km, důkladně prověřil,
jestli nám v nohách zbyly nějaké motorické funkce. Něco jsme ještě
z rezerv vyškrábli a tak jsme po 9 hodinách a 36 minutách, absolvovaných
53 km a s 2.440 nastoupanými metry, opět spočinuli na parkovišti
v Jánských Lázních. 🎉

## Zhodnocení

Bylo to náročné, to bez debat. Trošku mě zarazil nesoulad mezi odhadem
potřebného času a časem skutečným --- jeden a půl hodiny je docela dost.
Přemýšlím, jestli to bylo dáno dodatečným převýšením, tím, že jsem méně
trénoval, tím že jsem od podzimu 10 kg přibral, tím, že to byl individuální
a ne hromadný závod... 🤔 Asi všechno dohromady.

Každopádně, je to trasa, kterou stojí za to zkusit. Určitě si ji někdy
dám ještě jednou, jen tentokrát v opačném gardu.

## Mapa a profil

{{< figure src="/2021/img/cerna-snezka/Map-and-profile.png"
    link="/2021/img/cerna-snezka/Map-and-profile.png" >}}

## Související články

* [Sněžka 25k](/2020/snezka-25k/)
* [Krkonošská padesátka, můj první (baby) ultra trail](/2020/krkonoska-padesatka/)
