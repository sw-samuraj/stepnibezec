+++
title = "Sněžné jámy, Kozí hřbety 30k"
date = 2021-07-09T11:29:39+02:00
tags = ["trail", "vrcholy", "krkonoše"]
description = """Proběhnutí po hřebenech Krkonoš v délce 30 km."""
thumbnail = "2021/img/snezne-jamy/Kozi-hrbety.jpg"
+++

{{< figure src="/2021/img/snezne-jamy/Kozi-hrbety-header.jpg"
    link="/2021/img/snezne-jamy/Kozi-hrbety-header.jpg" >}}

Za tímhle běžckým výletem stála jedna fotka --- prosluněný den, malebná stavba
nad útesem... zjistil jsem si, co to je a že prej Krkonoše, Sněžné jámy.
Nikdy jsem tam nebyl a protože v rámci rodinné turistiky bych se tam (zatím)
nedostal, naplánoval jsem trailovou trasu, která vyšla na nějakých 30 km.

Společníka mi v Krkonoších již potřetí dělal Mr. Bear, 🐻 tudíž o svižnější
přůběh trasy bylo postaráno. Počasí vypadalo proměnlivě, takže jsme úplně
nevěděli, co nahoře čekat, ale vzhledem k tomu, že již uděřil červenec,
vyrazili jsme v 10:01 ze Špindlerova Mlýna jen v kraťasech a v tričku.

{{< figure src="/2021/img/snezne-jamy/Labsky-dul.jpg"
    link="/2021/img/snezne-jamy/Labsky-dul.jpg"
    caption="Labský důl" >}}

Samozřejmě, nejsme lehkomyslní, takže v běžeckých baťůžcích jsme měli
přibalenou větrovku a triko navíc. Já jsem měl tradičně běžecký hole,
4 energetický gely (na každých 8 km) a novinku --- místo softflasků jsem
si vzal čerstvě pořízený 2l bidon.

Takže kromě toho, že jsem se chtěl podívát na místa v Krkonoších, kde
jsem ještě nebyl, chtěl jsem taky vyzkoušet, jak se běhá se 2 l vody
(zatím jsem sebou brával jen litr ve dvou půllitrových softflascích).

{{< figure src="/2021/img/snezne-jamy/Vysoke-kolo.jpg"
    link="/2021/img/snezne-jamy/Vysoke-kolo.jpg"
    caption="Pod Vysokým kolem, v pozadí Mr. Bear 🐻" >}}

Ze Špindlu jsme běželi po modré do Labského dolu a navzdory tomu, že jsem
v noci špatně spal, jsem běžel docela svižně. Dokonce tak svižně, že jsem
vyběhnul chodníčkem kamenných serpentýn až k Labské boudě a ani mě nenapadlo
vytáhnout hůlky.

To mě docela překvapilo, protože se mi úplně nestává, že bych čiperně
běžel, pokud je na cca 8 km převýšení 550 m. Ale prostě mi pro tentokrát
běžecký Manitou nadělil a Mr. Bear 🐻 to náležitě ocenil.

{{< figure src="/2021/img/snezne-jamy/Snezne-jamy.jpg"
    link="/2021/img/snezne-jamy/Snezne-jamy.jpg"
    caption="Sněžné jámy (ta nejméně zajímavá část)" >}}

Jestliže v Labském dole bylo počasí pod mrakem, tak od Labské jsme byli
v mracích. Ke sněžným jámám to bylo jenom dalších 1,5 km a tak jsem si
říkal, že to nevypadá moc nadějně --- že to místo, kvůli kterému jsem to
naplánoval nejspíš vůbec neuvidím.

Trochu jsem se utěšoval, že třeba se na mě usměje štěstí a vítr foukne
do mraků a my něco spatříme, nicméně nestalo se tak --- oblačnost byla
hustá jako mlíko a tak jsme bez zdržování pokračovali směrem na východ,
po Hřebenovce směř Sněžka.

{{< figure src="/2021/img/snezne-jamy/Stepni-bezec-a-Mr-Bear.jpg"
    link="/2021/img/snezne-jamy/Stepni-bezec-a-Mr-Bear.jpg"
    caption="Stepní běžec 🐺 & Mr. Bear 🐻" >}}

Tady si musím postěžovat --- jakkoliv jsou Krkonoše krásné a rád v nich
běhám či chodím, tak nesnáším ty časté kamenné chodníčky (zejména
na hřebenech), po kterých se dost blbě běhá. Je to o-zlom-kotník
a člověk musí pořád dávat pozor, aby nehodil držku. Což v závěru dlouhých
běhů už není vůbec snadné.

Proto mě potěšilo, že při seběhu pod Vysokým kolem byl kamený chodníček
tvořen pouze dvojicí velikých plochých balvanů (viz foto výše), po kterých
se krásně plynule běželo. Měli jsme za sebou zhruba 10 km a naše nohy měly
křídla.

{{< figure src="/2021/img/snezne-jamy/Muzske-kameny.jpg"
    link="/2021/img/snezne-jamy/Muzske-kameny.jpg"
    caption="Mužské kameny" >}}

Mírně zvlněným hřebenem jsme minuli malebné Mužské a Dívčí kameny, následované
ne až tak záživným seběhem kolem Petrovy boudy až ke Špindlerově boudě.
Tam už jsme byli lehce za půlkou.

Následný výstup na Malý Šišák mi dal trošičku zabrat --- sice jsem si tak
nějak maloval, byvše při síle, že bych mohl dát celý běh úplně bez hůlek,
ale kousek za úpatím tohohle kopce jsem si přiznal, že bych to zbytečně rval
a není k tomu důvod. Pokorně jsem uvolnil poutka a zbytek kopce vyhůlkoval.

{{< figure src="/2021/img/snezne-jamy/Divci-kameny.jpg"
    link="/2021/img/snezne-jamy/Divci-kameny.jpg"
    caption="Dívčí kameny" >}}

Nahoře už to bylo veselejší a tak jsem hůlky zase zapasoval (a do konce běhu
už je nevytáhnul). Už od Mužských kamenů se udělalo hezké běžecké počasí,
slunečno, občas chladný větřík a následujících 7 km po polské straně byla
snadno ubíhající cesta, zpestřená krásnými plesy Velkého a Malého Stawu.

Tady už se na mně začala projevovat únava a tak, když jsme na 23. km
překročili hranici zpátky do Česka, už se mi do mírného kopce na Luční
boudu moc běžet nechtělo. Nicméně Mr. Bear 🐻 říkal, že od Luční už je to
zadarmo, tak jsem ještě trochu zabral.

{{< figure src="/2021/img/snezne-jamy/Snezka.jpg"
    link="/2021/img/snezne-jamy/Snezka.jpg"
    caption="Sněžka z polské strany" >}}

Na Luční jsme si dali energetickou přestávku --- přesně podle rozpisu jsem
si dal na 24. km třetí gel a vyrazili jsme po rovince na Kozí hřbety a to
už moje nohy moc běžet nechtěly. Uvést je do chodu šlo dost ztuha.

Když jsme míjeli Rennerovu studánku, řekl jsem si, že bych měl zkontrolovat
stav bidonu. A dobře jsem udělal, byl prázdný. Tohle budu muset ještě
vyladit --- u softflasků můžu pohmatem jednoduše kontrolovat, kolik vody
mi zbývá, ale u bidonu zatím nevím, jak to zjistit bez sundavání vesty.

{{< figure src="/2021/img/snezne-jamy/Kociol-Wielkiego-Stawu.jpg"
    link="/2021/img/snezne-jamy/Kociol-Wielkiego-Stawu.jpg"
    caption="Kocioł Wielkiego Stawu" >}}

Doplnil jsem cca litřík pramenité vody a během chvilky jsme se přeloupli
přes hřeben Hřbetů na jejich jižní stranu --- ten pohled na jejich zubaté
ostří stojí za to. Škoda, že se tam nedá jít.

Na hřebínku jsme symbolicky proťali 25. km a čekal nás už jen 6km seběh
do Špindlu po krásném, lehce technickém single trailu. Dlouhé a technické
seběhy jsou moje silná stránka, takže jsem si to užíval --- unava nohou
ze mě spadla a já jsem skotačil dolů jak mladý jelen. 🦌

{{< figure src="/2021/img/snezne-jamy/Kociol-Malego-Stawu.jpg"
    link="/2021/img/snezne-jamy/Kociol-Malego-Stawu.jpg"
    caption="Kocioł Małego Stawu, v pozadí Studniční hora" >}}

Tady, v samotném závěru došlo taky k jediné nehodě našeho výběhu ---
Mr. Bear, 🐻 který běžel za mnou, nedával chvilku pozor a vzápětí už
jsem slyšel neklamné zvuky, které vydává běžecké salto mortale. Naštestí
se nic vážného nestalo a stržená kůže na dlani je přijatelná běžecká trofej.

Těsně nad Špindlem se lehce rozpršelo a tak jsme si mohli gratulovat, jak nám
parádně vyšlo počasí. Navázali jsme na silničku, propletli se skrumáží penzionů
a byli jsme zpátky na parkovišti. Svaly na nohou příjemně bolely, hlava krásně
vyčištěná a tak už jsme se jen upínali k příjemné představě dobré kávy. ☕
Well done! 👍

{{< figure src="/2021/img/snezne-jamy/Kozi-hrbety.jpg"
    link="/2021/img/snezne-jamy/Kozi-hrbety.jpg"
    caption="Kozí hřbety" >}}

Řečí čísel jsme dnes uběhli 31,27 km za 4:38 a nasbírali 1.225 m převýšení.
Vybíhali jsme z nadmořské výšky 748 m n.m. (Špindlerův Mlýn) a nejvyšší bod
naší trasy probíhal výškou 1.490 m n.m. (Sněžné jámy).

## GPS trasa 

* [Trasa na Mapy.cz](//mapy.cz/s/gafanumuru)
* [Garmin course](//connect.garmin.com/modern/course/63626949)
* [Strava route](//www.strava.com/routes/2850395775368114480)

{{< figure src="/2021/img/snezne-jamy/Elevation.jpg"
    link="/2021/img/snezne-jamy/Elevation.jpg"
    caption="Profil převýšení" >}}

## Související články

* [Sněžka 25k](/2020/snezka-25k/)
* [FKT Černá Sněžka](/2021/fkt-cerna-snezka/)
* [Krkonošská padesátka, můj první (baby) ultra trail](/2020/krkonoska-padesatka/)
