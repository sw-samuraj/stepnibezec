+++
title = "Noční varianta FKT Černá Sněžka"
date = 2021-07-25T17:09:35+02:00
tags = ["fkt", "závody", "ultra", "trail", "vrcholy", "krkonoše"]
description = """Vymyslel jsem si pro FKT Černá Sněžka noční variatu a tak
    jsem ji vyzkoušel."""
thumbnail = "2021/img/nocni-cerna-snezka/Running-kit.jpg"
+++

{{< figure src="/2021/img/nocni-cerna-snezka/Cerna-hora.jpg"
    link="/2021/img/nocni-cerna-snezka/Cerna-hora.jpg" >}}

Když jsem před měsícem dával [rozhovor pro server Sprortigo.cz](//www.sportigo.cz/rozhovor-vyhovuje-mi-meditacni-povaha-dlouhych-trati/),
ještě jsem o téhle záležitosti nic netušil. A pak to přišlo. Možná to
souviselo s nákupem rezervní čelovky pro [PVLH24](//www.pvlh24.cz/),
nebo s tím, že jsem projížděl různé výzvy na [Fastest Known
Time](//fastestknowntime.com/), prostě mě to napadlo ---
udělat _noční variantu_ [FKT Černá Sněžka](//cernasnezka.cz).

Vymyslet to bylo jednoduchý, teď už to jen zrealizovat. A když říkám
zrealizovat, myslím tím: sám si to vyzkoušet, zaběhnout si to.

## Co to je _Noční varianta_ FKT Černá Sněžka?

Co to je [FKT Černá Sněžka](//cernasnezka.cz) a jak jsem ji premiérově
absolvoval [jsem psal](/2021/fkt-cerna-snezka/) už před měsícem.
Nicméně v krátkosti, pro ty, co nechtějí klikat na odkazy: je to
50km okruh v Krkonoších, kdy se běží z Černé hory na Sněžku a zpátky
(jinou trasou). Převýšení dělá pěkných 2.300 m a je potřeba to zvládnout
do 12 hodin. Navíc, jsou tam ještě nějaká další
[pravidla](//cernasnezka.cz/pravidla).

_Noční varianta_ je pak totéž, ale je potřeba vystartovat (celoročně)
ve 22:00 a celou trasu urazit v noci. Zbytek pravidel je stejný, v ničem
jiném se to neliší.

## Výbava

Výbavu jsem měl hodně podobnou tomu, když jsem běžel _Černou Sněžku_ minule,
ale pár rozdílů tam bylo. Tak především, měl jsem dvě čelovky. Obě jsou
_Fenix_, jedna je aktuální novinka, speciálně pro trail running navržená
[HL18R-T](//www.kronium.cz/nabijeci-celovka-fenix-hl18r-t/prod_2371.html)
a druhá je "brunnerovka" [HM50R](//www.kronium.cz/nabijeci-celovka-fenix-hm50r/prod_1806.html).

Další změnou bylo, že jsem chtěl běžet stylem _unsupported_, tak jak ho
definují [FKT Guidelines](//fastestknowntime.com/guidelines), takže
ke dvěma softflaskům jsem přidal 2l bidon na vodu. Celkem jsem tedy
měl 3 litry vody.

Třetí drobná změna byla, že jsem si nebral žádné náhradní a dodatečné
oblečení --- přece jen, šlo o letní noc s rozumnou předpovědí, takže
jsem se oblíknul jako na chladnější běh v horách: slabé dlouhé elasťáky,
dvě trika (s krátkým, s dlouhým) a větrovku.

Zbytek už byl klasika --- hůlky na kopce, maličkou powerbanku na hodinky,
či mobil a 6 energetických gelů. Boty jsem si vzal _Inov-8 TerraUltra G 270_,
které jsem měl vyzkoušený z minulého (kratšího) výběhu v Krkonoších:
[Sněžné jámy, Kozí hřbety 30k](/2021/snezne-jamy-kozi-hrbety-30k/).

{{< figure src="/2021/img/nocni-cerna-snezka/Running-kit.jpg"
    link="/2021/img/nocni-cerna-snezka/Running-kit.jpg" >}}

## Počasí

Jak se říká, počasí na horách je vrtkavé. Původně jsem měl běh naplánovaný
o den dřív, ale jak mi na telefon začaly chodit notifikace "yellow warning
for thunderstorms" a "red warning for thunderstorms" (oranžové a červené
varování před bouřkami), tak jsem své úmysly přehodnotil a vyčkal
na příhodnější noc.

Dobře jsem udělal, protože následné bouřky po celém Česku způsobily
leckde i záplavy a v samotných Krkonoších se blýskalo jak nad Tatrami.

Během plánování s ohledem na počasí jsem udělal dobrou zkušenost
se stránkou [Výstražné informace](//www.chmi.cz/files/portal/docs/meteo/om/vystrahy/index.html)
na webu Českého hydrometeorologického ústavu. Doporučuji!

Následující noc (kdy jsem vyrazil) skýtala nejistou předpověď
ohledně deště, ale jinak standardní počasí na horách, bez silného
větru a velkých teplotních výkyvů, zhruba něco nad 10 °C.

## Zabezpečení

Vydat se na noc do hor je samozřejmě riskantní. Vydat se na noc
do hor úplně sám je... ještě víc riskantní. Takže jsem přirozeně
přemýšlel, jak se zabezpečit.

Jedna věc je mít odpovídající výbavu. To jsem měl (viz výše,
i když [izotermická fólie](//cs.wikipedia.org/wiki/Izotermick%C3%A1_f%C3%B3lie)
by taky neuškodila). Druhá věc je mít bezpečnostní kontakt (nebo víc kontaktů),
který ví kam se vydáváte a kde se zrovna nacházíte.

Mým bezpečnostním kontaktem byla manželka, se kterou jsme trávili
bez dětí týden na horách. Na několika kratších tůrách, které běhu
předcházely, jsem si vyzkoušel, jaké sledovací nástroje nabízí
_Garmin Connect_ a _Strava_. Fungovalo to dobře, takže pak už na ostro
jsem ani já, ani žena, neměli žádné obavy.

### Garmin Live Track & Live Event Sharing

{{< figure src="/2021/img/nocni-cerna-snezka/Garmin-Live-Event-Sharing.jpg"
    link="/2021/img/nocni-cerna-snezka/Garmin-Live-Event-Sharing.jpg"
    class="floatright" width="300" >}}

_Garmin Connect_ (na mobilu) má dva bezpečnostní nástroje a výhodou je, že jsou
oba zdarma (jen musíte mít nějaké _Garmin_ zařízení).

Jedním je _Live Track_.
Nastaví se jeden či více kontaktů, na které se mailem odešle odkaz, kde je pak
možné běžce živě sledovat. _Live Track_ se dá zapnout buď manuálně přímo v telefonu,
nebo se dá nastavit, že se zapne při startu aktivity na hodinkách. Zároveň je možné
to spárovat i se _Strava Beacon_ (viz níže), tj. spuštění _Live Track_ spustí
zároveň i _Strava Beacon_.

Druhým nástrojem je _Live Event Sharing_. Opět se nastaví příjemce, kterým pak
_Garmin_ posílá SMS podle nastavených parametrů. Zpráva se posílá buď
po uražení určité intervalové vzdálenosti (zdá se, že maximální interval je
zvláštních 8 km, že by 5 mil?), nebo po časovém úseku. Funguje to docela dobře, jen
je potřeba si zkontrolovat, že zpráva opravdu odešla --- pokud jste v místech,
kde není signál SMS se nepodaří odeslat. Během _Černé Sněžky_ se mi to stalo
u jedné zprávy.

### Strava Beacon

_Strava Beacon_ funguje hodně obdobně jako _Garmin Live Track_. S tím rozdílem,
že odkaz se posílá SMS na zvolené telefonní číslo a samotné sledování se spouští
startem aktivity přímo ve _Strava_ aplikaci na mobilu. Další rozdíl je, že sledování
(_Beacon_) zůstane uložené a lze se k němu poději vrátit (oproti _Live Track_, který
je k dispozici jenom 24 hodin po skončení aktivity).

{{< figure src="/2021/img/nocni-cerna-snezka/Strava-Beacon.png"
    link="/2021/img/nocni-cerna-snezka/Strava-Beacon.png" >}}

### Aplikace Záchranka

Kdybych běžel o týden později, mohl jsem využít další bezpečnostní službu,
protože aplikace [Záchranka](//www.zachrankaapp.cz/) čerstvě uvedla novou
funkcionalitu pro přivolání [Horské služby](//www.horskasluzba.cz/) a možnost
zadat si túru do registru Horské služby: [Elektronická kniha
tůr](//www.zachrankaapp.cz/cs/jak-aplikaci-pouzivat#horskasluzba).
Je dobré vědět, vyzkouším příště.

## Report z trasy

Ubytování jsme měli v Peci pod Sněžkou, kde jsme si zašli na večeři --- já
jsem si dal obligátní běžecké špagety (tuším boloňské) a aby mě to před během
pořádně nakoplo a měl jsem dost energie, dal jsem si na závěr crème brûlée
a espresso.

Žena mě na parkovišti v Jánských Lázních vyložila lehce před desátou večerní,
dochystal jsem výbavu, zapózoval pro předstartovní foto a v souladu
s pravidly (22:00 ± 15 minut) jsem ve 22:08 vyrazil na trasu.

Tentokrát jsem volil směr proti směru hodinových ručiček, takže mě čekal
prudký výstup až na vrchol Černé hory. Jestliže v Jánských byla (noční)
viditelnost dobrá, tak s nabíranou výškou jsem se zanořil do mlhy (či mraků?)
husté tak, že nebylo vidět dál, než na 3-5 metrů.

{{< figure src="/2021/img/nocni-cerna-snezka/Start.jpg"
    link="/2021/img/nocni-cerna-snezka/Start.jpg"
    caption="Předstartovní foto" >}}

Oproti minule jsem byl striktní a nehrál si na hrdinu --- kopce zásadně chodit
(s hůlkama) a běhat jenom rovinky a z kopce. S hůlkama jsem stylem power hike
ukusoval výškové metry takovou intenzitou, že jsem záhy sundal větrovku a dalších
skoro 20 km šel a běžel jen v tričkách.

Dostat se na vrchol Černé hory znamená zdolat 5 km a nějakých 600 m převýšení.
Já jsem se tam dostal za hodinu. Mlha byla tak hustá, že jsem sice bez problémů
našel vrchní stanici lanovky, ale chvíli jsem bloudil, než jsem našel cestu
k rozhledně, kde jsem se vyfotil pro vrcholové foto.

Následný 7km seběh do Temného Dolu nebyl ničím moc zajímavý. Běželo se krásně,
bylo to z kopce (opět 600 m výškových, tentokrát dolů) a trvalo to další hodinku.
Při seběhu jsem si vychutnal první gel --- tradičně jsem je měl rozplánované
po osmi kilometrech.

{{< figure src="/2021/img/nocni-cerna-snezka/Cerna-hora.jpg"
    link="/2021/img/nocni-cerna-snezka/Cerna-hora.jpg"
    caption="Na vrcholu Černé hory (čas 23:18)" >}}

Zpestřením cesty z Temného Dolu na Dlouhý hřeben je pěkná moderní křížová
cesta ke kapli sv. Anny, ale když to člověk jde takhle ze spoda, je to
pěkný trhák a člověk si to moc neužije. Samotný Dlouhý hřeben je dlouhý...
a nudný.

Někde tady už jsem se s čelovkou HL18R-T plně skamarádil a vyvinul jsem si
následující rutinu --- při power hiku jsem svítil na nejnižší řežim 70
lumenů, při běhu jsem přepínal na o stupeň silnějších 200 lumenů. Takhle
mi to vyhovovalo. Nejsilnější režim 500 lumenů jsem nikdy nepoužil,
taková palba mi přišla zbytečná.

Prvním zpestřením této části cesty byly až Pomezní Boudy, kam jsem
dorazil kolem druhé hodiny v noci. Hodinky ukazovaly 24 km a tedy
čas na třetí gel. Odtud už to začaly být ty pravé Krkonoše ---
prvně stoupání do Sovího sedla a následně výstup na Svorovou horu.

{{< figure src="/2021/img/nocni-cerna-snezka/Snezka.jpg"
    link="/2021/img/nocni-cerna-snezka/Snezka.jpg"
    caption="Na vrcholu Sněžky (čas 3:54)" >}}

Cesta na Svorovou horu je tvořená kamenným chodníčkem, plným vysokých
schodů --- ty mě solidně potrápily. Byl jsem už 5 hodin na cestě,
v nohách 28 kilometrů a to už bylo na stehenních svalech dost znát.
Díky bohu za hůlky, bez nich by bylo tohle stoupání festovním utrpením.

Nahoře na Svorovce jsem potkal první živé duše --- kluk a holka šli
z Pomezek na Sněžku. Chvilku jsem s nimi pokecal a běžel dál. Tuhle část
Hřebenovky nemám kvůli kamennému chodníku moc rád, ale jakž takž běžet
se dalo.

Tady už se měnilo počasí --- mlha/mraky byly pěkně hustý, foukal chladný
a vytrvalý vítr, naštěstí ne až tak nepříjemný a hlavně, nezpomalující
pohyb vpřed. Co vám budu povídat... výhledy nebyly, tak jsem se probojovával
mlhou na Sněžku.

{{< figure src="/2021/img/nocni-cerna-snezka/Lisci-hora.jpg"
    link="/2021/img/nocni-cerna-snezka/Lisci-hora.jpg"
    caption="Liščí hora v raní mlze (čas 5:26)" >}}

Lidí začalo přibývat, buď párečky, nebo skupinky. Někteří byli rozumně
oblečeni, někteří moc ne. Asi šli na východ slunce na Sněžku, ale nevím
--- vzhledem k předpovědi těžko mohli čekat, že nějaké slunce uvidí.

Na Sněžku jsem dorazil krátce před čtvrtou hodinou ranní s mezníkem 31. km.
Fičelo tam nepěkně, ale navzdory počasí tam bylo docela živo. Fakt nevím,
co si od toho ti lidé slibovali. Pošolichal jsem vrcholovou fotku a
po řetězech valil dolů, obdivován protijdoucími.

Někde dole, už cestou na polskou stranu jsem vypnul čelovku. Bylo půl
páté. Sice ještě pořád hustá mlha, ale už světlo. Myslím, že s ní můžu
být spokojen --- čelovka běžela ve výše popsaném střídavém režimu celou
noc, to jest 6,5 hodiny a nějaký čas by určitě ještě vydržela.

{{< figure src="/2021/img/nocni-cerna-snezka/Cerna-hora-morning.jpg"
    link="/2021/img/nocni-cerna-snezka/Cerna-hora-morning.jpg"
    caption="Černá hora nad ránem (čas 5:44)" >}}

Dobouchal jsem to na Luční boudu a k Památníku obětem hor. Teď už to
bude krásně běhatelný a pořád z kopce... až před závěrečnou Černou
horou. Výrovka, Na Rozcestí, Lišcí hora. Mlha se začala zvolna rozplývat.
Konečně už se dalo něco smysluplně fotit.

Tady už mi bylo jasný, že kolem osmi hodin to dneska nedám (i když
na Sněžce to ještě tak vypadalo), ale pořád ještě jsem si maloval,
že bych to mohl zvládnout kolem osm a půl hodiny. Prostě jako vždycky,
milosrdně (či naivně?) jsem zapomněl, co mě ještě čeká na Černé hoře.

Závěrečný seběh do údolí říčky Čistá a stoupání do úbočí Černé hory
byla už trochu agónie --- únava, bolavý nohy, ne až tak zajímavá
krajina a hlavně... silnice. Závěrečný 6km seběh do Jánských Lázní
byl dlouhý, velmi dlouhý. Zpátky na parkoviště jsem doběhnul krátce
po čtvrt na osm ráno, v celkovém čase 9:08:29.

## Shrnutí

Pokud to vezmu z hlediska FKT, vylepšil jsem jak svůj osobák, tak
oficiální FKT o 28 minut. Reálně jsem to nešel rychleji --- při prvním
pokusu jsme si 1,5 km zaběhli a navíc jsme si dávali na Pomezkách
polívku. Takže co se týče rychlosti, šel jsem to více méně obdobně rychle.
Samozřejmě, teď jsem šel v noci, takže některé techničtější useky jsem
šel pomaleji a bezpečněji, na jistotu.

Když to vezmu z hlediska zkušeností, byl to můj první běh přes noc.
Zajímavá a cenná zkušenost --- pěkně mi to sedlo, absence spánku mi
nijak nevadila. Zajímalo by mě, jestli když se jde něco delšího,
dejme tomu nějaká stovka, jestli mě pak spánkový deficit během
následujícího dne nedožene.

Dobrá byla taky zkušenost s čelovkou. Nijak mi při běhu nevadila, jak
v monotónním, tak v technickém terénu mě podržela a výdrž baterie
protentokrát slušná (i když uvidím jak to bude s nějakým delším použitím,
dejme tomu 8 hodin a víc).

Taky jsem si ověřil, že 50 km se dá jít jenom na gely. Nicméně příště
to určitě proložím ještě něčím dalším --- tady budu muset ještě experimentovat,
protože různé tyčinky a jiné ultra-svačinky ještě nemám prozkoumané.

Tím, že jsem šel v noci a bylo chladno, jsem bez problémů vystačil
jen se 3 litry vody. Ve dne bych určitě spotřeboval víc a za teplého
počasí ještě víc --- na loňské [Krkonošské padesátce](/2020/krkonoska-padesatka/)
jsem v horku vypil přes 5 litrů tekutin. Spokojený jsem s 2l
bidonem --- na zádech krásně sedí a velikost vs. váha mi přijde
tak akorát.

Ačkoliv to byla už druhá letošní padesátka, beru to hlavně jako trénink
--- původně jsem měl v letos v plánu [PVLH24](//www.pvlh24.cz/), ale asi
to přeložím na příští rok. Místo toho bych si ale rád dal ještě něco
delšího. Líbilo by se mi něco v délce 50 mil (80 km). Tak uvidím.

## Související články

* [FKT Černá Sněžka](/2021/fkt-cerna-snezka/)
* [Krkonošská padesátka, můj první (baby) ultra trail](/2020/krkonoska-padesatka/)
